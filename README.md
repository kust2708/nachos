# NachOS Project #

## Installation ##
To compile and run Nachos on your computer, you need some developpement packages (to compile 32 bits C++ code) that are present in any Linux distribution but you also need a MIPS cross compiler.

The archive contains a script (called README.Debian, run it with "bash README.Debian" after reading it) that should be able to install the required software on a Debian or an Ubuntu machine in order to work on Nachos on your own machine.

## Subject ##

### Subproject 1 : Installing NachOS ### 
First Steps in NachOS : http://imag-moodle.e.ujf-grenoble.fr/pluginfile.php/5364/mod_resource/content/1/step1.pdf

### Subproject 2 : IOs ###
Assignement : http://imag-moodle.e.ujf-grenoble.fr/pluginfile.php/5363/mod_resource/content/0/etape2EN.pdf

NachOS syscalls : http://imag-moodle.e.ujf-grenoble.fr/pluginfile.php/5510/mod_resource/content/0/nachos-syscall.pdf

### Subproject 3 : Multithreading ###
Presentation : http://imag-moodle.e.ujf-grenoble.fr/pluginfile.php/5512/mod_resource/content/0/ETAPE3.pdf

NachOS Multithreading : http://imag-moodle.e.ujf-grenoble.fr/pluginfile.php/5369/mod_resource/content/0/etape3_EN.pdf

### Subproject 4 : NachOS Virtual Memory ###
Presentation : http://imag-moodle.e.ujf-grenoble.fr/pluginfile.php/8314/mod_resource/content/1/ETAPE4_2012.pdf

Assignement : http://imag-moodle.e.ujf-grenoble.fr/pluginfile.php/5595/mod_resource/content/0/etape4EN.pdf

### Subproject 5 : NachOS File System ###
Assignement : http://imag-moodle.e.ujf-grenoble.fr/pluginfile.php/5666/mod_resource/content/0/etape5EN.pdf

### Subproject 6 : NachOS Network ###
Assignement : http://imag-moodle.e.ujf-grenoble.fr/pluginfile.php/5677/mod_resource/content/0/etape6EN.pdf

### Evaluation  ###
Prepare your defense : http://imag-moodle.e.ujf-grenoble.fr/pluginfile.php/5701/mod_resource/content/0/Soutenances_NACHOS_EN_2010_2011.key.pdf