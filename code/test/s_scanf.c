#include "syscall.h"

int main()
{
	int n1, n2, n3;
	char ch;
	char str[16];
	
	Printf("Please enter %d integers\n", 3);
	int ret = Scanf("%d %d %d", &n1, &n2, &n3);
	Printf("%d numbers are read\n", ret);
	Printf("The numbers are [%d,%d,%d]\n", n1, n2, n3);
	
	Printf("Now enter a char, integer and a string\n");
	Scanf("%c %d %s", &ch, &n1, str);
	Printf("You entered: ch=%c, int=%d, str=%s\n", ch, n1, str);

	return 0;
}