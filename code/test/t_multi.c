#include "syscall.h"

void f2(void *arg) {
	/*int val = *((int *) arg);
	SynchPutChar('#');
	SynchPutInt(val);
	SynchPutString("#\n");*/

	SynchPutString("##########################################################################\n");

	UserThreadExit(0);
}


void f1(void *arg) {
	int y = 2;
	/*int val = *((int *) arg);
	SynchPutChar('<');
	SynchPutInt(val);
	SynchPutString(">\n");*/
	//Halt();
	SynchPutString("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

	int pid_t2 = UserThreadCreate(f2, (void*)&y);
	//Halt();
	UserThreadJoin(pid_t2);

	//Halt();

	UserThreadExit(0);
}

int main() {
	int x = 4, pid_t1/*, pid_t2*/;

    pid_t1 = UserThreadCreate(f1, (void*)&x);
    //pid_t2 = UserThreadCreate(f2, (void*)&y);

    SynchPutString(">>>>>>>>>>>>>> Before Join <<<<<<<<<<<<<<\n");

    UserThreadJoin(pid_t1);
    //UserThreadJoin(pid_t2);

    SynchPutString(">>>>>>>>>>>>>> After Join <<<<<<<<<<<<<<\n");

	return 0;
}
