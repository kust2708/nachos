#include "syscall.h"

#define MAX_THREADS 100

sem_t mutex;

int C = 0;

void increment(void* arg) {
	C++;
}

void safe_increment(void* arg) {
	P(&mutex);
	C++;
	V(&mutex);
}

int main() {

	int tid[MAX_THREADS], i;

	SynchPutString("At the begining\n");
	Printf("The value of the global variable = %d\n", C);

	SemInit(&mutex, 1);

	for (i = 0; i < MAX_THREADS; ++i)
	{
		// tid[i] = UserThreadCreate(increment, 0);
		tid[i] = UserThreadCreate(safe_increment, 0);
	}

	for (i = 0; i < MAX_THREADS; ++i)
	{
		UserThreadJoin(tid[i]);
	}

	SemDestroy(&mutex);

	SynchPutString("At the end\n");
	Printf("The value of the global variable = %d\n", C);

	return !(C == MAX_THREADS);
}
