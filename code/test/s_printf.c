#include "syscall.h"

int main()
{
	Printf("%s [4+%c=%d]\n", "Is it true?", '7', 13);
	Printf("Wrong %f flags %i like %x this %s and this %s\n", "%f", "%x");
	int written_args = Printf("Too many arguments [%d,%d,%d,%d,%d,%d]\n", 1, 2, 3, 4, 5, 6);
	Printf("#of well written arguments = %d\n", written_args);

	return 0;
}
