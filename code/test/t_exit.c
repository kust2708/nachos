#include "syscall.h"

void f(void *arg) {
	int val = *((int *) arg);
	SynchPutChar('[');
	SynchPutInt(val);
	SynchPutString("]\n");	
	//UserThreadExit(2);
	//return (void)2;
}

int main() {
	int x = 4, y = 8, z = 16;
	int id1 = UserThreadCreate(f, (void*)&x);
	int id2 = UserThreadCreate(f, (void*)&y);
	int id3 = UserThreadCreate(f, (void*)&z);
	UserThreadJoin(id1);
	UserThreadJoin(id2);
	UserThreadJoin(id3);
	SynchPutString(">>>>>>>>>>> The End <<<<<<<<<<\n");
	return 0;
}
