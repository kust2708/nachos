#include  "syscall.h"

int main()
{
	int pid1 = ForkExec("p_userpages0");
	int pid2 = ForkExec("p_userpages1");

	Join(pid1);
	Join(pid2);

	SynchPutString("--{End of the main process}--\n");
	return 0;
}
