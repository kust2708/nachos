#include  "syscall.h"

int main()
{
	SynchPutString("--{Begin of the main process}--\n");

	Join(ForkExec("p_userpages0"));
	SynchPutString("\n--{End of the first process}--\n");
	Join(ForkExec("p_userpages1"));
	SynchPutString("\n--{End of the second process}--\n");

	SynchPutString("--{End of the main process}--\n\n");
	
	return 0;
}
