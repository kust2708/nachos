#include "syscall.h"


#define MAX_THREADS 1000

void func(void *arg) {
	int val = *((int*) arg) % 26;
	SynchPutChar('a' + val);
}

int main() {

	int i;
	for (i=0; i<MAX_THREADS; i++)
	{
		int tid = UserThreadCreate(func, (void*)&i);
    	UserThreadJoin(tid);	
	}
	SynchPutChar('\n');
    
	return 0;
}
