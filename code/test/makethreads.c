#include "syscall.h"

void sqr(void *arg) {
	int val = *((int *) arg);
	int val2 = val * val;
	SynchPutString(">> Inside sqr:: The parameter is [");
	SynchPutInt(val);
	SynchPutString("]\n");
	UserThreadExit(val2);
}

int main() {
	int x = 4;
	SynchPutString(">> Inside main:: Creating a thread..\n");
	SynchPutString(">> Inside main:: .. with a func='sqr' and param='");
	SynchPutInt(x);
	SynchPutString("'\n");
    int tid = UserThreadCreate(sqr, (void*)&x);
	SynchPutString(">> Inside main:: Waiting for thread to finish..\n");
    int res = UserThreadJoin(tid);
	SynchPutString(">> Inside main:: The returned value from the thread is [");
	SynchPutInt(res);
	SynchPutString("]\n");
	return 0;
}
