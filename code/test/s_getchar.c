#include "syscall.h"

int main()
{
	while(1)
	{
		char c = SynchGetChar();
		if(c == -1)
			break;
		SynchPutChar(c);
	}
	return 0;
}