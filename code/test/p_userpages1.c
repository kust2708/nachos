#include  "syscall.h"

#define THIS "ccc"
#define THAT "ddd"

const int N = 100;  //  Choose  it  large  enough!

void _puts(char *s)
{
	char *p; for (p = s; *p != '\0'; p++)
		SynchPutChar(*p);
		//Printf("\033[34;01m%c\033[30;01m", *p);
}

void f(void *s)
{
	int i; for (i = 0; i < N; i++) _puts((char *)s);
}

int main()
{
	int tid = UserThreadCreate(f, (void *) THIS);
	f((void*) THAT);
	UserThreadJoin(tid);
	return 0;
}
