#include "syscall.h"

#define BUFFER_SIZE 3

int buff[BUFFER_SIZE];

sem_t mutex;
sem_t fillCount;
sem_t emptyCount;

int head=0, tail=0;

int C = 0;

int produce_item() {
	SynchPutString("\033[34;01mProducing: [");
	SynchPutInt(C);
	SynchPutString("]\033[00m\n");
	return C++;
}
void consume_item(int n) {
	SynchPutString("\033[31;01mConsuming: [");
	SynchPutInt(n);
	SynchPutString("]\033[00m\n");
}

void push(int n) {
	buff[tail] = n;
	tail = (tail+1)%BUFFER_SIZE;
}

int pop() {
	int v = buff[head];
	head = (head+1)%BUFFER_SIZE;
	return v;
}

void producer(void* arg) {
    while (1) {
        P(&emptyCount);
            P(&mutex);
				int item = produce_item();
                push(item);
            V(&mutex);
        V(&fillCount);
    }
}
 
void consumer(void* arg) {
    while (1) {
        P(&fillCount);
            P(&mutex);
                int item = pop();
				consume_item(item);
            V(&mutex);
        V(&emptyCount);
    }
}

void init() {
	SemInit(&mutex, 1);
	SemInit(&fillCount, 0);
	SemInit(&emptyCount, BUFFER_SIZE);
}

int main() {

	int pid_t1, pid_t2;

	init();

	pid_t1 = UserThreadCreate(producer, 0);
	pid_t2 = UserThreadCreate(consumer, 0);

	//SynchPutString("-------------- Before Join --------------\n");

	UserThreadJoin(pid_t1);
	UserThreadJoin(pid_t2);

	//SynchPutString("-------------- After Join --------------\n");
	return 0;
}
