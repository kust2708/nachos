#include "syscall.h"

void f3(int i3) {
	int r3 = i3 + 7;
	SynchPutInt(r3);
    SynchPutChar('#');
}

void f2(int i2) {
	int l1 = 5;
	int r2 = l1 + i2;
	SynchPutInt(r2);
    SynchPutChar('#');
	f3(r2);
}

void f1(int i1) {
	int l1 = 1, l2 = 2;
	int r1 = l1 + l2;
	SynchPutInt(r1);
    SynchPutChar('#');
	f2(r1);
}

int main() {
	SynchPutInt(-1);
	int x = 4;
	f1(x);
    SynchPutChar('#');
	SynchPutInt(x);
	SynchPutString("#\n");
	return 0;
}
