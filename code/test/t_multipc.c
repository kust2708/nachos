#include "syscall.h"

#define BUFFER_SIZE 3

int buff[BUFFER_SIZE];

sem_t mutex;
sem_t fillCount;
sem_t emptyCount;

int head=0, tail=0;

int C = 0;

int produce_item(int id) {
	SynchPutString("\033[34;01mProducing<");
	SynchPutInt(id);
	SynchPutString(">: [");
	SynchPutInt(C);
	SynchPutString("]\n");
	return C++;
}
void consume_item(int id, int n) {
	SynchPutString("\033[31;01mConsuming<");
	SynchPutInt(id);
	SynchPutString(">: [");
	SynchPutInt(n);
	SynchPutString("]\n");
}

void push(int n) {
	buff[tail] = n;
	tail = (tail+1)%BUFFER_SIZE;
}

int pop() {
	int v = buff[head];
	head = (head+1)%BUFFER_SIZE;
	return v;
}

void producer(void* arg) {
    int id = *((int*)arg);
    while (1) {
        P(&emptyCount);
            P(&mutex);
				int item = produce_item(id);
                push(item);
            V(&mutex);
        V(&fillCount);
    }
}
 
void consumer(void* arg) {
    int id = *((int*)arg);
	while (1) {
        P(&fillCount);
            P(&mutex);
                int item = pop();
				consume_item(id, item);
            V(&mutex);
        V(&emptyCount);
    }
}

void init() {
	SemInit(&mutex, 1);
	SemInit(&fillCount, 0);
	SemInit(&emptyCount, BUFFER_SIZE);
}

int main() {
	
	int _1 = 1, _2 = 2, _3 = 3;

	init();

	UserThreadCreate(producer, &_1);
	UserThreadCreate(producer, &_2);
	UserThreadCreate(producer, &_3);

	UserThreadCreate(consumer, &_1);
	UserThreadCreate(consumer, &_2);
	UserThreadCreate(consumer, &_3);

	return 0;
}
