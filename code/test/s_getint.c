#include "syscall.h"

int main()
{
	int n = 0, p;
	while(1)
	{
		p = SynchGetInt(&n);
		if(n == -1) {
			SynchPutString("The End.\n");
			break;
		}
		if(!p) {
			SynchPutString("You entered q chqrqcter.\n");
			break;
		}
		SynchPutChar('[');
		SynchPutInt(n);
		SynchPutString("]\n");
	}
	return 0;
}