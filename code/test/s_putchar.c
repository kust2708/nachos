#include "syscall.h"

void print(char c, int n)
{
	int i;
	for (i = 0; i < n; i++) {
		SynchPutChar(c+i);
	}
	SynchPutChar('\n');
}

int main()
{
	print('a',10);
	print('b',7);
	//Halt();
	//Cleanup();
	return 0;
	//Exit(0);
}
