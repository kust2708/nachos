// filehdr.cc 
//	Routines for managing the disk file header (in UNIX, this
//	would be called the i-node).
//
//	The file header is used to locate where on disk the 
//	file's data is stored.  We implement this as a fixed size
//	table of pointers -- each entry in the table points to the 
//	disk sector containing that portion of the file data
//	(in other words, there are no indirect or doubly indirect 
//	blocks). The table size is chosen so that the file header
//	will be just big enough to fit in one disk sector, 
//
//      Unlike in a real system, we do not keep track of file permissions, 
//	ownership, last modification date, etc., in the file header. 
//
//	A file header can be initialized in two ways:
//	   for a new file, by modifying the in-memory data structure
//	     to point to the newly allocated data blocks
//	   for a file already on disk, by reading the file header from disk
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "filehdr.h"

//----------------------------------------------------------------------
// FileHeader::Allocate
// 	Initialize a fresh file header for a newly created file.
//	Allocate data blocks for the file out of the map of free disk blocks.
//	Return FALSE if there are not enough free blocks to accomodate
//	the new file.
//
//	"freeMap" is the bit map of free disk sectors
//	"fileSize" is the bit map of free disk sectors
//----------------------------------------------------------------------

bool
FileHeader::Allocate(BitMap *freeMap, int fileSize) {
    IntStatus oldLevel = interrupt->SetLevel(IntOff);
    numBytes = fileSize;
    int Num_curSec = 0;
    int Num_exSec = 0;
    int i, j;
    bool result = true;
    int L1_Lev_Buf[NumL1Ind];
    int L2_Lev_Buf[NumL1Ind];
    int Total_Sector[NumDirect + NumL1Ind + NumL1Ind * NumL1Ind + 34];
    int getNum = 0;
    L1Index = -1;
    L2Index = -1;
    numSectors = divRoundUp(fileSize, SectorSize);
    if ((unsigned) numSectors > NumDirect) {
        if ((unsigned) numSectors > NumDirect + NumL1Ind) {
            if ((unsigned) numSectors > NumDirect + NumL1Ind + NumL1Ind * NumL1Ind) //>max sector index
                result = false;
            else {
                Num_exSec = divRoundUp(numSectors - NumDirect - NumL1Ind, NumL1Ind);
                if (freeMap->NumClear() < 2 + numSectors + Num_exSec) //L2 index check
                    result = false;
                else {
                    for (i = 0; i < 2 + numSectors + Num_exSec; i++)
                        Total_Sector[ i ] = freeMap->Find();
                }
            }
        } else {
            if (freeMap->NumClear() < numSectors + 1) //L1 index check
                result = false;
            else {
                for (i = 0; i < 1 + numSectors; i++)
                    Total_Sector[ i ] = freeMap->Find();
            }
        }
    } else {
        if (freeMap->NumClear() < numSectors) //Direct check
            result = false;
        else {
            for (i = 0; i < numSectors; i++)
                Total_Sector[ i ] = freeMap->Find();
        }
    }
    (void) interrupt->SetLevel(oldLevel);
    if (result == false) {
        return false;
    }
    //allocate sector
    for (i = 0; (unsigned) i < NumDirect && Num_curSec < numSectors; i++, Num_curSec++) { //Direct
        dataSectors[ i ] = Total_Sector[ getNum++ ];
        Clean_Sec(dataSectors[ i ]);
    }
    if (Num_curSec < numSectors) { //L1
        L1Index = Total_Sector[ getNum++ ];
        Clean_Sec(L1Index);
        for (i = 0; (unsigned) i < NumL1Ind; i++)
            L1_Lev_Buf[ i ] = -1;
        for (i = 0; (unsigned) i < NumL1Ind && Num_curSec < numSectors; i++, Num_curSec++) {
            L1_Lev_Buf[ i ] = Total_Sector[ getNum++ ];
            Clean_Sec(L1_Lev_Buf[ i ]);
        }
        synchDisk->WriteSector(L1Index, (char *) L1_Lev_Buf);
        if (Num_curSec < numSectors) { //L2
            L2Index = Total_Sector[ getNum++ ];
            Clean_Sec(L2Index);
            for (i = 0; (unsigned) i < NumL1Ind; i++)
                L2_Lev_Buf[ i ] = -1;
            for (j = 0; j < Num_exSec; j++) {
                L2_Lev_Buf[ j ] = Total_Sector[ getNum++ ];
                Clean_Sec(L2_Lev_Buf[ j ]);
                for (i = 0; (unsigned) i < NumL1Ind; i++)
                    L1_Lev_Buf[ i ] = -1;
                for (i = 0; (unsigned) i < NumL1Ind && Num_curSec < numSectors; i++, Num_curSec++) {
                    L1_Lev_Buf[ i ] = Total_Sector[ getNum++ ];
                    Clean_Sec(L1_Lev_Buf[ i ]);
                }
                synchDisk->WriteSector(L2_Lev_Buf[ j ], (char *) L1_Lev_Buf);
            }
            synchDisk->WriteSector(L2Index, (char*) L2_Lev_Buf);
        }
    }
    return true;
}

void
FileHeader::Deallocate(BitMap *freeMap) {
    IntStatus oldLevel = interrupt->SetLevel(IntOff);
    int Num_curSec = 0;
    int i, j;
    int L1_Lev_Buf[NumL1Ind];
    int L2_Lev_Buf[NumL1Ind];
    for (i = 0; (unsigned) i < NumDirect && Num_curSec < numSectors; i++, Num_curSec++) { //Direct
        ASSERT(freeMap->Test((int) dataSectors[i]));
        freeMap->Clear((int) dataSectors[i]);
    }
    if (Num_curSec < numSectors) { //L1
        ASSERT(freeMap->Test(L1Index));
        synchDisk->ReadSector(L1Index, (char*) L1_Lev_Buf);
        for (i = 0; (unsigned) i < NumL1Ind && Num_curSec < numSectors; i++, Num_curSec++) {
            ASSERT(freeMap->Test(L1_Lev_Buf[ i ]));
            freeMap->Clear(L1_Lev_Buf[ i ]);
        }
        if (Num_curSec < numSectors) { //L2
            ASSERT(freeMap->Test(L2Index));
            synchDisk->ReadSector(L2Index, (char*) L2_Lev_Buf);
            for (j = 0; Num_curSec < numSectors; j++) {
                ASSERT(freeMap->Test(L2_Lev_Buf[ j ]));
                synchDisk->ReadSector(L2_Lev_Buf[ j ], (char*) L1_Lev_Buf);
                for (i = 0; (unsigned) i < NumL1Ind && Num_curSec < numSectors; i++, Num_curSec++) {
                    ASSERT(freeMap->Test(L1_Lev_Buf[ i ]));
                    freeMap->Clear(L1_Lev_Buf[ i ]);
                }
                freeMap->Clear(L2_Lev_Buf[ j ]);
            }
            freeMap->Clear(L2Index);
        }
        freeMap->Clear(L1Index);
    }
    (void) interrupt->SetLevel(oldLevel);
}


//----------------------------------------------------------------------
// FileHeader::FetchFrom
// 	Fetch contents of file header from disk. 
//
//	"sector" is the disk sector containing the file header
//----------------------------------------------------------------------

void
FileHeader::FetchFrom(int sector) {
    synchDisk->ReadSector(sector, (char *) this);
}

//----------------------------------------------------------------------
// FileHeader::WriteBack
// 	Write the modified contents of the file header back to disk. 
//
//	"sector" is the disk sector to contain the file header
//----------------------------------------------------------------------

void
FileHeader::WriteBack(int sector) {
    synchDisk->WriteSector(sector, (char *) this);
}

//----------------------------------------------------------------------
// FileHeader::ByteToSector
// 	Return which disk sector is storing a particular byte within the file.
//      This is essentially a translation from a virtual address (the
//	offset in the file) to a physical address (the sector where the
//	data at the offset is stored).
//
//	"offset" is the location within the file of the byte in question
//----------------------------------------------------------------------

int
FileHeader::ByteToSector(int offset) {
    int Num_curSec = offset / SectorSize;
    int L1_Lev_Buf[NumL1Ind];
    int L2_Lev_Buf[NumL1Ind];
    int adSector = 0;
    int offSector = 0;
    if (Num_curSec < 0)
        return -1;
    if ((unsigned) Num_curSec < NumDirect)
        return ( dataSectors[ Num_curSec ]);
    else if ((unsigned) Num_curSec < NumDirect + NumL1Ind) {
        synchDisk->ReadSector(L1Index, (char *) L1_Lev_Buf);
        return L1_Lev_Buf[ Num_curSec - NumDirect ];
    } else if ((unsigned) Num_curSec < NumDirect + NumL1Ind + NumL1Ind * NumL1Ind) {
        adSector = ((unsigned) Num_curSec - NumDirect - NumL1Ind) / NumL1Ind;
        offSector = ((unsigned) Num_curSec - NumDirect - NumL1Ind) - adSector * NumL1Ind;
        synchDisk->ReadSector(L2Index, (char *) L2_Lev_Buf);
        synchDisk->ReadSector(L2_Lev_Buf[ adSector ], (char *) L1_Lev_Buf);
        return L1_Lev_Buf[ offSector ];
    }
    return -1;
}


//----------------------------------------------------------------------
// FileHeader::FileLength
// 	Return the number of bytes in the file.
//----------------------------------------------------------------------

int
FileHeader::FileLength() {
    return numBytes;
}

//----------------------------------------------------------------------
// FileHeader::Print
// 	Print the contents of the file header, and the contents of all
//	the data blocks pointed to by the file header.
//----------------------------------------------------------------------

void
FileHeader::Print() {
    int i, j, k;
    int loop1, loop2;
    int Num_curSec = 0;
    int L1_Lev_Buf[NumL1Ind];
    int L2_Lev_Buf[NumL1Ind];
    char *data = new char[SectorSize];
    printf("FileHeader contents.  File size: %d.  File blocks:\n", numBytes);

    for (i = 0; (unsigned) i < NumDirect && Num_curSec < numSectors; i++, Num_curSec++) { //Direct
        printf("%d ", dataSectors[i]);
    }
    if (Num_curSec < numSectors) { //L1
        synchDisk->ReadSector(L1Index, (char*) L1_Lev_Buf);
        for (i = 0; (unsigned) i < NumL1Ind && Num_curSec < numSectors; i++, Num_curSec++) {
            printf("%d ", L1_Lev_Buf[ i ]);
        }
        if (Num_curSec < numSectors) { //L2
            synchDisk->ReadSector(L2Index, (char*) L2_Lev_Buf);
            for (j = 0; Num_curSec < numSectors; j++) {
                synchDisk->ReadSector(L2_Lev_Buf[ j ], (char*) L1_Lev_Buf);
                for (i = 0; (unsigned) i < NumL1Ind && Num_curSec < numSectors; i++, Num_curSec++) {
                    printf("%d ", L1_Lev_Buf[ i ]);
                }
            }
        }
    }
    Num_curSec = 0;
    for (i = k = 0; (unsigned) i < NumDirect && Num_curSec < numSectors; i++, Num_curSec++) { //Direct
        synchDisk->ReadSector(dataSectors[i], data);
        for (j = 0; (j < SectorSize) && (k < numBytes); j++, k++) {
            if ('\040' <= data[j] && data[j] <= '\176') // isprint(data[j])
                printf("%c", data[j]);
            else
                printf("\\%x", (unsigned char) data[j]);
        }
        printf("\n");
    }
    if (Num_curSec < numSectors) { //L1
        synchDisk->ReadSector(L1Index, (char*) L1_Lev_Buf);
        for (loop1 = k = 0; (unsigned) loop1 < NumL1Ind && Num_curSec < numSectors; loop1++, Num_curSec++) { //L1
            synchDisk->ReadSector(L1_Lev_Buf[ loop1 ], data);
            for (j = 0; (j < SectorSize) && (k < numBytes); j++, k++) {
                if ('\040' <= data[j] && data[j] <= '\176') // isprint(data[j])
                    printf("%c", data[j]);
                else
                    printf("\\%x", (unsigned char) data[j]);
            }
            printf("\n");
        }
        if (Num_curSec < numSectors) { //L2
            synchDisk->ReadSector(L2Index, (char*) L2_Lev_Buf);
            for (loop2 = 0; Num_curSec < numSectors; loop2++) {
                synchDisk->ReadSector(L2_Lev_Buf[ loop2 ], (char*) L1_Lev_Buf);
                for (loop1 = k = 0; (unsigned) loop1 < NumL1Ind && Num_curSec < numSectors; loop1++, Num_curSec++) {
                    synchDisk->ReadSector(L1_Lev_Buf[ loop1 ], data);
                    for (j = 0; (j < SectorSize) && (k < numBytes); j++, k++) {
                        if ('\040' <= data[j] && data[j] <= '\176') // isprint(data[j])
                            printf("%c", data[j]);
                        else
                            printf("\\%x", (unsigned char) data[j]);
                    }
                    printf("\n");
                }
            }
        }
    }
    delete [] data;
}

bool FileHeader::Dy_allocate(BitMap *freeMap, int newSize) {
    if ((unsigned) newSize > MaxFileSize)
        return false;
    if (newSize <= numBytes)
        return true;
    IntStatus oldLevel = interrupt->SetLevel(IntOff);

    int Total_Sector[NumDirect + NumL1Ind + NumL1Ind * NumL1Ind];
    int new_Sectors = divRoundUp(newSize, SectorSize);
    int Num_curSec;
    int totalNum_curSecs = 0;
    int Num_exSec;
    int i, j;
    int getNum = 0;
    int L1_Lev_Buf[NumL1Ind];
    int L2_Lev_Buf[NumL1Ind];
    bool result = true;

    if (new_Sectors <= numSectors) {
        (void) interrupt->SetLevel(oldLevel);
        numBytes = newSize;
        return true;
    }
    if ((unsigned) numSectors > NumDirect) {
        if ((unsigned) numSectors > NumDirect + NumL1Ind) {
            if ((unsigned) numSectors > NumDirect + NumL1Ind + NumL1Ind * NumL1Ind) //>max sector index
                result = false;
            else {
                Num_exSec = divRoundUp(numSectors - NumDirect - NumL1Ind, NumL1Ind);
                totalNum_curSecs = numSectors + Num_exSec + 2;
            }
        } else {
            totalNum_curSecs = numSectors + 1;
        }
    } else {
        totalNum_curSecs = numSectors;
    }

    if ((unsigned) new_Sectors > NumDirect) {
        if ((unsigned) new_Sectors > NumDirect + NumL1Ind) {
            if ((unsigned) new_Sectors > NumDirect + NumL1Ind + NumL1Ind * NumL1Ind) //>max sector index
                result = false;
            else {
                Num_exSec = divRoundUp(new_Sectors - NumDirect - NumL1Ind, NumL1Ind);
                if (freeMap->NumClear() < 2 + new_Sectors + Num_exSec - totalNum_curSecs) //L2 index check
                    result = false;
                else {
                    for (i = 0; i < 2 + new_Sectors + Num_exSec - totalNum_curSecs; i++)
                        Total_Sector[ i ] = freeMap->Find();
                }
            }
        } else {
            if (freeMap->NumClear() < new_Sectors + 1 - totalNum_curSecs) //L1 index check
                result = false;
            else {
                for (i = 0; i < 1 + new_Sectors - totalNum_curSecs; i++)
                    Total_Sector[ i ] = freeMap->Find();
            }
        }
    } else {
        if (freeMap->NumClear() < new_Sectors - totalNum_curSecs) //Direct check
            result = false;
        else {
            for (i = 0; i < new_Sectors - totalNum_curSecs; i++)
                Total_Sector[ i ] = freeMap->Find();
        }

    }
    if (result == false) {
        (void) interrupt->SetLevel(oldLevel);
        return false;
    }
    Num_curSec = numSectors;
    numSectors = new_Sectors;
    numBytes = newSize;
    DEBUG('v', "Cur Exist Sector:%d, NewSectorNum:%d\n", Num_curSec, numSectors);
    (void) interrupt->SetLevel(oldLevel);
    //allocate sector
    for (i = Num_curSec; (unsigned) i < NumDirect && Num_curSec < numSectors; i++, Num_curSec++) { //Direct
        dataSectors[ i ] = Total_Sector[ getNum++ ];
        Clean_Sec(dataSectors[ i ]);
        DEBUG('v', "Insert New Direct Sector:%d in dataSectors[ %d ]\n", dataSectors[i], i);
    }
    if (Num_curSec < numSectors) { //L1
        if (L1Index == -1) {
            L1Index = Total_Sector[ getNum++ ];
            Clean_Sec(L1Index);
            for (i = 0; (unsigned) i < NumL1Ind; i++)
                L1_Lev_Buf[ i ] = -1;
            DEBUG('v', "Insert New L1Index:%d\n", L1Index);
        } else {
            synchDisk->ReadSector(L1Index, (char*) L1_Lev_Buf);
        }
        for (i = Num_curSec - NumDirect; (unsigned) i < NumL1Ind && Num_curSec < numSectors; i++, Num_curSec++) {
            ASSERT(L1_Lev_Buf[ i ] == -1);
            L1_Lev_Buf[ i ] = Total_Sector[ getNum++ ];
            Clean_Sec(L1_Lev_Buf[ i ]);
            DEBUG('v', "Insert New L1 Sector:%d in L1_Lev_Buf[ %d ]\n", L1_Lev_Buf[i], i);
        }
        synchDisk->WriteSector(L1Index, (char *) L1_Lev_Buf);
        if (Num_curSec < numSectors) { //L2
            if (L2Index == -1) {
                L2Index = Total_Sector[ getNum++ ];
                Clean_Sec(L2Index);
                for (i = 0; (unsigned) i < NumL1Ind; i++)
                    L2_Lev_Buf[ i ] = -1;
                DEBUG('v', "Insert New L2Index:%d\n", L2Index);
            } else
                synchDisk->ReadSector(L2Index, (char*) L2_Lev_Buf);
            for (j = (Num_curSec - (NumDirect + NumL1Ind)) / NumL1Ind; Num_curSec < numSectors; j++) {
                if (L2_Lev_Buf[ j ] == -1) {
                    L2_Lev_Buf[ j ] = Total_Sector[ getNum++ ];
                    Clean_Sec(L2_Lev_Buf[ j ]);
                    for (i = 0; (unsigned) i < NumL1Ind; i++)
                        L1_Lev_Buf[ i ] = -1;
                    DEBUG('v', "Insert New L2 Index:%d in L2_Lev_Buf[ %d ]\n", L2_Lev_Buf[j], j);
                } else
                    synchDisk->ReadSector(L2_Lev_Buf[ j ], (char*) L1_Lev_Buf);
                for (i = (Num_curSec - (NumDirect + NumL1Ind)) - (j * NumL1Ind); (unsigned) i < NumL1Ind && Num_curSec < numSectors; i++, Num_curSec++) {
                    ASSERT(L1_Lev_Buf[ i ] == -1);
                    L1_Lev_Buf[ i ] = Total_Sector[ getNum++ ];
                    Clean_Sec(L1_Lev_Buf[ i ]);
                    DEBUG('v', "Insert New L2 Sector:%d in L1_Lev_Buf[ %d ]\n", L1_Lev_Buf[i], i);
                }
                synchDisk->WriteSector(L2_Lev_Buf[ j ], (char *) L1_Lev_Buf);
            }
            synchDisk->WriteSector(L2Index, (char*) L2_Lev_Buf);
        }
    }
    return true;

}

void FileHeader::Clean_Sec(int sectorNum) {
    char allZero[SectorSize] = {0};
    synchDisk->WriteSector(sectorNum, (char *) allZero);
}
