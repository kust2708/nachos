#ifndef FPOST_H
#define FPOST_H

#include "copyright.h"
#include "post.h"


class FPostOffice
{
	public:
		FPostOffice() {};
		~FPostOffice() {};
		//Send a message to a mailbox on a remote machine.  The fromBox in the MailHeader is the return box for ack's.
		int SendRequest(PacketHeader pktHdr, MailHeader mailHdr, const char *data);
		// Send a file from a machine to another one using network 
		int SendFile(PacketHeader pktHdr, MailHeader mailHdr, const char *data);
		// Retrieve a message from "box".  Wait if there is no message in the box.
		void ReceiveRequest(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char *data);
		// Receive a file from another machine using network
		void ReceiveFile(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char *fname);
};

#endif // FPOST_H
