#include "fpost.h"
#include "system.h"
#include "filehdr.h"
#include <time.h>

/**
 * Send a new file send request
 * pktHdr : Header of the packet
 * mailHdr : Header of the mail (here the file)
 * fname : file name 
*/
int FPostOffice::SendRequest(PacketHeader pktHdr, MailHeader mailHdr, const char *fname)
{
	DEBUG('r', "FPostOffice SendRequest\n");
	postOffice->Send(pktHdr, mailHdr, fname);
	return 0;
}

/**
 * Send a message to a mailbox on a remote machine.
 * pktHdr : Header of the packet
 * mailHdr : Header of the mail (here the file)
 * fname : file name 
 */
int FPostOffice::SendFile(PacketHeader pktHdr, MailHeader mailHdr, const char *fname)
{
	DEBUG('r', "FPostOffice Send\n");
	OpenFile *openFile;    
    if ((openFile = fileSystem->Open(fname)) == NULL) {
		printf("Print: unable to open file %s\n", fname);
		return 0;
    }
    int fsize = openFile->Length();
    char *fbuffer = new char[fsize];
    openFile->Read(fbuffer, fsize);
	mailHdr.length = fsize;

	clock_t t1 = clock(); // initial time
	int val = vPostOffice->Send(pktHdr, mailHdr, fbuffer);
	float total_time = ((clock() - t1) / 1000000.0F ) * 1000; // Send request time
	printf("%.6f Kb/s\n", fsize/(total_time*1024)); // transfert speed calculation

	delete openFile;
	delete [] fbuffer;
	return val;
}

/**
 * Receive a request from a mailbox on a remote machine.
 * box : Mailbox id
 * pktHdr : Header of the packet
 * mailHdr : Header of the mail (here the file)
 * data : this pointer will contains the received data
 */
void FPostOffice::ReceiveRequest(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char *data)
{
	DEBUG('r', "FPostOffice ReceiveRequest\n");
	postOffice->Receive(box, pktHdr, mailHdr, data);
}

/**
 * Retrieve a message from "box".  Wait if there is no message in the box.
 * box : Mailbox id
 * pktHdr : Header of the packet
 * mailHdr : Header of the mail (here the file)
 * data : this pointer will contains the received data
 */
void FPostOffice::ReceiveFile(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char* fname)
{
	DEBUG('r', "FPostOffice Receiving..\n");
	char* content = new char[MaxFileSize];
	vPostOffice->Receive(box, pktHdr, mailHdr, content);
	DEBUG('r', "FPostOffice Received.\n");

	int len = strlen(content) + 1;
	DEBUG('r', "FileName=[%s], Content=[%s], HeadLength=[%d], len=[%d]\n", fname, content, mailHdr->length, len);
	// create new file
    if (!fileSystem->Create(fname, len)) {	 // Create Nachos file
		printf("Copy: couldn't create output file %s\n", fname);
		return;
    }
    OpenFile* openFile = fileSystem->Open(fname);
    ASSERT(openFile != NULL);
    
	openFile->Write(content, len);
    delete content;
    delete openFile;

	printf("received\n");
}
