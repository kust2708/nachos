#include "vpost.h"
#include "system.h"

/**
 * Send a string to a mailbox on a remote machine.
 * Take care of the type, this function works only for char* type.
 * If you want send another type of data, please see the below function (SendBytes)
 * pktHdr : Header of the packet
 * mailHdr : Header of the mail (here the file)
 * data : this pointer will contains the string
 */
int VPostOffice::Send(PacketHeader pktHdr, MailHeader mailHdr, const char *data)
{
	DEBUG('r', "VPostOffice Send\n");

	return SendBytes(pktHdr, mailHdr, data, strlen(data));
}

/**
 * Send bytes to a mailbox on a remote machine. Can send any type of data 
 * pktHdr : Header of the packet
 * mailHdr : Header of the mail (here the file)
 * data : this pointer will contains the string
 * total_size : size of the data which must be sent
 */
int VPostOffice::SendBytes(PacketHeader pktHdr, MailHeader mailHdr, const char *data, long long int total_size)
{
	DEBUG('r', "VPostOffice SendBytes\n");

	int nb_pkg = divRoundUp(total_size, MaxVMailSize);
	VPostHeader vHdr(nb_pkg);

	DEBUG('r', "Total size=%d\n", total_size);
	DEBUG('r', "#Of packages=%d\n", nb_pkg);
	
	char* buffer = new char[MaxRMailSize];
	bcopy(&vHdr, buffer, sizeof(VPostHeader));

	vHdr.print("sent");
	
	for(int i=0; i<nb_pkg; i++)
	{
		DEBUG('r', "Sending part %d\n", i);
		mailHdr.length = MaxVMailSize;
		if (i == nb_pkg - 1) {
			mailHdr.length = (unsigned char) (total_size - i * MaxVMailSize);
		}
		bcopy(data + i * MaxVMailSize, buffer + sizeof(VPostHeader), mailHdr.length);

		mailHdr.length += sizeof(VPostHeader);
		int val = rPostOffice->Send(pktHdr, mailHdr, buffer);
		if(!val)
			return 0;
	}
	delete [] buffer;
	return 1;
}

/**
 * Retrieve a message from "box".  Wait if there is no message in the box.
 * box : Mailbox id
 * pktHdr : Header of the packet
 * mailHdr : Header of the mail (here the file)
 * data : this pointer will contains the received data
 */
void VPostOffice::Receive(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char *data)
{
	DEBUG('r', "VPostOffice Receiving..\n");
	char* buff = new char[MaxRMailSize];	// space to hold concatenated
	VPostHeader vHdr(0);
	int nb_pkg = 0, total_size = 0;

	do
	{
		rPostOffice->Receive(box, pktHdr, mailHdr, buff);

		mailHdr->length -= sizeof(VPostHeader);
		bcopy(buff, &vHdr, sizeof(VPostHeader));
		bcopy(buff + sizeof(VPostHeader), data + total_size, mailHdr->length);
		DEBUG('r', "Part %d is received\n", nb_pkg);
		printf("[%s]\n", data);
		nb_pkg++;
		total_size += mailHdr->length;
		DEBUG('r', "total received bytes = %d\n", total_size);
	}
	while(vHdr.nb_packages > nb_pkg);
	mailHdr->length = total_size;
	delete buff;
	vHdr.print("received");
}
