#include "copyright.h"

#include "system.h"
#include "network.h"
#include "post.h"
#include "interrupt.h"
#include "fpost.h"


extern void Print (const char *file);
extern void Copy (const char *unixFile, const char *nachosFile);


#define FILE_BOX 5 // file box id


void TestFileTransfer(int machine_addr, int server_addr)
{
	if (machine_addr == server_addr)
	{
		Copy ("../filesys/test/small", "small");
		Copy ("../filesys/test/medium", "medium");
		Copy ("../filesys/test/big", "big");
		printf("The file system of the server\n");
		fileSystem->List();
		printf("\n");
		
		printf("The server is running!\n");
		while (1)
		{
			PacketHeader outPktHdr, inPktHdr;
			MailHeader outMailHdr, inMailHdr;
			char fname[64];

			printf("Waiting for a request..\n");

			fPostOffice->ReceiveRequest(FILE_BOX, &inPktHdr, &inMailHdr, fname);

			printf("A request received to dowload file [%s]\n", fname);

			// headers definition
			outPktHdr.to = inPktHdr.from;
			outMailHdr.to = inMailHdr.from;
			outMailHdr.from = FILE_BOX;
			outMailHdr.length = 0;

			if (!strcmp(fname, "$$$"))
			{
				// TODO
				// fPostOffice->SendAvailableFiles(outPktHdr, outMailHdr);	
			}
			else
			{
				fPostOffice->SendFile(outPktHdr, outMailHdr, fname);
			}

			printf("The file [%s] is sent\n", fname);
		}
	}
	else
	{
		char Files[3][64] = {"small", "medium", "big"}; // the name of the file on the server
	
		// The client
		printf("Machine [%d]\n", machine_addr);
		//sleep(machine_addr);
					
		PacketHeader outPktHdr, inPktHdr;
		MailHeader outMailHdr, inMailHdr;
		char fname[64]; // the name of the file on the server
		strcpy(fname, Files[machine_addr%3]);
		char path[64] = "my_"; // the path where to save the file locally
		strcat(path, fname);

		int boxId = 1 + machine_addr;
		printf("Requesting to dowload file [%s]\n", fname);
		
		outPktHdr.to = server_addr;
		outMailHdr.to = FILE_BOX;
		outMailHdr.from = boxId;
		outMailHdr.length = strlen(fname) + 1;

		fPostOffice->SendRequest(outPktHdr, outMailHdr, fname); 

		fPostOffice->ReceiveFile(boxId, &inPktHdr, &inMailHdr, path);

		printf("The file [%s] is downloaded\n", fname);
		
		printf("The new file system\n");
		fileSystem->List();
		printf("The content of the file\n");
		Print(path);
	}
}
