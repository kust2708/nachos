#ifndef VPOST_H
#define VPOST_H


#include "copyright.h"
#include "po_service.h"
#include "post.h"
#include "rpost.h"


struct VPostHeader {
	unsigned short int nb_packages;
	VPostHeader(int _nb_packages):nb_packages(_nb_packages){}

	void print(const char * name)
	{
		DEBUG('r', "VPostHeader::%s => (nb_packages) = (%d)\n", name, nb_packages); 
	}
};

#define MaxVMailSize 	(MaxRMailSize - sizeof(VPostHeader))


class VPostOffice : public PostOfficeService
{
	public:
		VPostOffice() {};
		~VPostOffice() {};
		//Send a message to a mailbox on a remote machine.  The fromBox in the MailHeader is the return box for ack's.
		int Send(PacketHeader pktHdr, MailHeader mailHdr, const char *data);
		// Retrieve a message from "box".  Wait if there is no message in the box.
		void Receive(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char *data);
		// Send bytes to a mailbox on a remote machine. (Can send any type of data)
		int SendBytes(PacketHeader pktHdr, MailHeader mailHdr, const char *data, long long int total_size);
};

#endif // VPOST_H
