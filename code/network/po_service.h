#ifndef ABSPOST_H
#define ABSPOST_H

#include "copyright.h"
#include "network.h"


// Mailbox address -- uniquely identifies a mailbox on a given machine.
// A mailbox is just a place for temporary storage for messages.
typedef unsigned char MailBoxAddress;

// The following class defines part of the message header.  
// This is prepended to the message by the PostOffice, before the message 
// is sent to the Network.

class MailHeader {
  public:
    MailBoxAddress to;		// Destination mail box
    MailBoxAddress from;	// Mail box to reply to
    unsigned char length;		// Bytes of message data (excluding the 
				// mail header)
};


/**
 * Abstract class that will defined an uniform API to work with all type
 * of communication.
 */ 
class PostOfficeService
{
	public:
		virtual ~PostOfficeService() {};
		//Send a message to a mailbox on a remote machine.  The fromBox in the MailHeader is the return box for ack's.
		virtual int Send(PacketHeader pktHdr, MailHeader mailHdr, const char *data) = 0;
		// Retrieve a message from "box".  Wait if there is no message in the box.
		virtual void Receive(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char *data) = 0;
};

#endif
