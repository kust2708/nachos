#include "rpost.h"
#include "system.h"

static int SN = 1;

/**
 * Warn the end of the reception
 * _rpo : RPostOffice pointer
*/
static void PacketReceived(int _rpo)
{ 
	RPostOffice *rpo = (RPostOffice *)_rpo; 
	rpo->aPacketReceived();
}

/**
 * Constructor of RPostOffice class
*/
RPostOffice::RPostOffice()
{
	lastSN = -1;
	packetAvailable = new Semaphore("RPostOffice lock", 0);
}

/**
 * Destructor of RPostOffice class
*/
RPostOffice::~RPostOffice()
{
	delete packetAvailable;
}

/**
 * Unlock the semaphore after reception of the packet
*/
void RPostOffice::aPacketReceived()
{
	packetAvailable->V();
}

/**
 * Send a message to a mailbox on a remote machine.
 * pktHdr : Header of the packet
 * mailHdr : Header of the mail
 * data : this pointer will contains the received data 
*/
int RPostOffice::Send(PacketHeader pktHdr, MailHeader mailHdr, const char *data)
{
	DEBUG('r', "RPostOffice Send\n");


    if (mailHdr.length > MaxRMailSize) {
        printf("The message is too long, send fail.");
        return -1;
    }

	RPostHeader rHdr;
	rHdr.set(SN++, 0); // init the RPostHeader
	rHdr.print("sent");
	
	// put the RPostHeader in the buffer
	char* buffer = new char[MaxMailSize];
	bcopy(&rHdr, buffer, sizeof(RPostHeader));
	bcopy(data, buffer + sizeof(RPostHeader), mailHdr.length);
	mailHdr.length += sizeof(RPostHeader);

	bool received = false;
	for (int i = 0; i < MAXREEMISSIONS && !received; ++i) // for each packets 
	{
		DEBUG('r', "Send the msg for the %d time\n", i);
		postOffice->Send(pktHdr, mailHdr, buffer);

		// wait a little bit before checking if the acknowledgment is present in the mailbox
		interrupt->Schedule(PacketReceived, (int)this, TEMPO, NetworkSendInt);
		packetAvailable->P();

		// acknowledgment reception
		if (postOffice->hasMsgs(ACK_MAIL_BOX)){
			DEBUG('r', "We have a new mail!!\n");
			char* ackBuf = new char[MaxPacketSize];
			PacketHeader ackPktHdr;
			MailHeader ackMailHdr;
			Receive(ACK_MAIL_BOX, &ackPktHdr, &ackMailHdr, ackBuf);
			RPostHeader ackHdr;
			bcopy(ackBuf, &ackHdr, sizeof(RPostHeader));
			ackHdr.print("received");
			if (ackHdr.sn() == rHdr.sn())
				received = true;
			delete ackBuf;
		}
	}
	delete buffer;

	if (DebugIsEnabled('r'))
	{
		if (!received)
			DEBUG('r', "The msg can't be send after %d try.\n", MAXREEMISSIONS);
		else
			DEBUG('r', "The mail has been sent successfully!!\n");
	}
	return received;
}


/**
 * Retrieve a message from "box".  Wait if there is no message in the box.
 * box : Mailbox id
 * pktHdr : Header of the packet
 * mailHdr : Header of the mail (here the file)
 * data : this pointer will contains the received data
 */
void RPostOffice::Receive(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char *data)
{
	DEBUG('r', "RPostOffice Receiving..\n");
	char* buff = new char[MaxPacketSize];	// space to hold concatenated
	RPostHeader rHdr;

	do
	{
		postOffice->Receive(box, pktHdr, mailHdr, buff);
		mailHdr->length -= sizeof(RPostHeader);

		bcopy(buff, &rHdr, sizeof(RPostHeader));
		rHdr.print("received");
	
		// This isan ack msg
		if (rHdr.ack()) {
			/* We have received the acknowledgement */ 
	
			bcopy(buff, data, sizeof(RPostHeader));
			delete buff;
			return ;
			
		} else {
			/* We have received a regular mail
			   Send back an acknowledgement mail */
	
			// fill the data from the packet.
			bcopy(buff + sizeof(RPostHeader), data, mailHdr->length);
	
			rHdr.ack_1();
			DEBUG('r', "A msg is received, send an ack\n");
			char* buffer = new char[sizeof(RPostHeader)];
			bcopy(&rHdr, buffer, sizeof(RPostHeader));
	
			MailHeader ackMailHdr;
			ackMailHdr.length = sizeof(RPostHeader);
			ackMailHdr.to = ACK_MAIL_BOX;
			ackMailHdr.from = mailHdr->to;
	
			PacketHeader ackHdr;
			ackHdr.to = pktHdr->from;
	
			postOffice->Send(ackHdr, ackMailHdr, buffer);
	
			delete buffer;
		}
	} while (rHdr.sn() <= lastSN);
	
	// update the last received packet SN
	lastSN = rHdr.sn();

	delete buff;
}
