#ifndef RPOST_H
#define RPOST_H


#include "copyright.h"
#include "po_service.h"
#include "synch.h"
#include "post.h"


#define TEMPO (8*1000*1000) // time to wait before resend.
#define MAXREEMISSIONS 10 // maximum iteration to try to resend
#define ACK_MAIL_BOX 0 // mail box id used for the acknowledgment, if you want send data, please use another mail box id


struct RPostHeader {
	int sn_ack;    // Hold the sequence number of the packet and the acknowledgment bit.

	void set(int _sn, bool _ack) {
		sn_ack = (_sn << 1) | _ack;
	}
	int sn() { return sn_ack >> 1; }
	bool ack() { return sn_ack & 1; }
	void ack_1() {  sn_ack |= 1; }
	void print(const char * name)
	{
		DEBUG('r', "RPostHeader::%s => (sn, ack) = (%d, %d)\n", name, sn(), ack()); 
	}
};

#define MaxRMailSize (MaxMailSize - sizeof(RPostHeader)) // maximum size which can be used to send data


class RPostOffice : public PostOfficeService
{
	public:
		RPostOffice();
		~RPostOffice();
		//Send a message to a mailbox on a remote machine.  The fromBox in the MailHeader is the return box for ack's.
		int Send(PacketHeader pktHdr, MailHeader mailHdr, const char *data);
		// Retrieve a message from "box".  Wait if there is no message in the box.
		void Receive(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char *data);
		// Warn the end of the reception
		void aPacketReceived();
	private:
		int lastSN; // is the last received packet SN 
		Semaphore* packetAvailable; // used for wait till TEMPO time is elapsed
};

#endif // RPOST_H
