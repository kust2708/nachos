// nettest.cc 
//	Test out message delivery between two "Nachos" machines,
//	using the Post Office to coordinate delivery.
//
//	Two caveats:
//	  1. Two copies of Nachos must be running, with machine ID's 0 and 1:
//		./nachos -m 0 -o 1 &
//		./nachos -m 1 -o 0 &
//
//	  2. You need an implementation of condition variables,
//	     which is *not* provided as part of the baseline threads 
//	     implementation.  The Post Office won't work without
//	     a correct implementation of condition variables.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"

#include "system.h"
#include "network.h"
#include "post.h"
#include "interrupt.h"


void PrintNetworkStatistics()
{
	printf("Max Wire Size = %d\n", MaxWireSize);
	printf("Max Packet Size = %d\n", MaxPacketSize);
	printf("Max Mail Size = %d\n", MaxMailSize);
	printf("Max RMail Size = %d\n", MaxRMailSize);
	printf("Max VMail Size = %d\n", MaxVMailSize);
}

// Test out message delivery, by doing the following:
//	1. send a message to the machine with ID "farAddr", at mail box #0
//	2. wait for the other machine's message to arrive (in our mailbox #0)
//	3. send an acknowledgment for the other machine's message
//	4. wait for an acknowledgement from the other machine to our 
//	original message

void
MailTest(int farAddr)
{
	const int max_itr = 10;

	PacketHeader outPktHdr, inPktHdr;
	MailHeader outMailHdr, inMailHdr;
	const char *data = "Hello there!";
	const char *ack = "Got it!";
	char buffer[MaxMailSize];

	// construct packet, mail header for original message
	// To: destination machine, mailbox 0
	// From: our machine, reply to: mailbox 1
	outPktHdr.to = farAddr;
	outMailHdr.to = 0;
	//outPktHdr.from = postOffice->netAddr;
	outMailHdr.from = 1;
	outMailHdr.length = strlen(data) + 1;
	for (int i=0; i<max_itr; i++)
	{
		// Send the first message
		postOffice->Send(outPktHdr, outMailHdr, data);
		// Wait for the first message from the other machine
		postOffice->Receive(0, &inPktHdr, &inMailHdr, buffer);
		printf("Got \"%s\" from %d to %d, box %d\n",buffer,inPktHdr.from, inPktHdr.to,inMailHdr.from);
		fflush(stdout);
	}
	// Send acknowledgement to the other machine (using "reply to" mailbox
	// in the message that just arrived
	outPktHdr.to = inPktHdr.from;
	outMailHdr.to = inMailHdr.from;
	outMailHdr.length = strlen(ack) + 1;
	for (int i=0; i<max_itr; i++)
	{
		postOffice->Send(outPktHdr, outMailHdr, ack); 
		// Wait for the ack from the other machine to the first message we sent.
		postOffice->Receive(1, &inPktHdr, &inMailHdr, buffer);
		printf("Got \"%s\" from %d to %d, box %d\n",buffer,inPktHdr.from, inPktHdr.to,inMailHdr.from);
		fflush(stdout);
	}

	// Then we're done!
	interrupt->Halt();
}

void
RingTest(int ring_size)
{
	//PostOfficeService *post = postOffice;
	PostOfficeService *post = rPostOffice;
	//PostOfficeService *post = vPostOffice;


	PacketHeader outPktHdr, inPktHdr;
	MailHeader outMailHdr, inMailHdr;
	char buffer[MaxMailSize];

	int addr = postOffice->getNetAddr();

	int boxid = 2;

	int next_addr = (addr + 1) % ring_size;
	int prev_addr = (addr - 1 + ring_size) % ring_size;

	printf("Machine address [%d]\n", addr);
	fflush(stdout);

	if (!addr)
	{
		const char *token = "Secret symbol [#]";

		printf("The genarated Token is {%s}\n", token);
		fflush(stdout);

		outPktHdr.to = next_addr;
		outMailHdr.to = boxid;
		outMailHdr.from = boxid;
		outMailHdr.length = strlen(token) + 1;

		printf("Send the token to address [%d]\n", next_addr);

		// Send the first message
		post->Send(outPktHdr, outMailHdr, token); 

		post->Receive(boxid, &inPktHdr, &inMailHdr, buffer);
		printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
		fflush(stdout);

		printf("Comparing the received token and the original token = (%d)\n", !strcmp(buffer, token));

		printf("The genarated Token is received again by the original sender.\n");
		fflush(stdout);
	}
	else
	{
		printf("Waiting the token from address [%d]\n", prev_addr);

		post->Receive(boxid, &inPktHdr, &inMailHdr, buffer);
		printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
		fflush(stdout);

		printf("Send the token to address [%d]\n", next_addr);

		outPktHdr.to = next_addr;
		outMailHdr.to = boxid;
		outMailHdr.from = boxid;
		outMailHdr.length = strlen(buffer) + 1;

		post->Send(outPktHdr, outMailHdr, buffer); 
	}

	// Then we're done!
	interrupt->Halt();
}

void
UnreliableNetworkTest(int farAddr)
{
	printf("Machine address [%d]\n", 1-farAddr);

	if (farAddr)
	{
		PacketHeader outPktHdr;
		MailHeader outMailHdr;
		const char *msg = "--{Hello there!}--";
	
		// construct packet, mail header for original message
		// To: destination machine, mailbox 0
		// From: our machine, reply to: mailbox 1
		outPktHdr.to = farAddr;
		outMailHdr.to = 7;
		outMailHdr.from = 1;
		outMailHdr.length = strlen(msg) + 1;
	
		printf("Sending a msg through an unreliable network\n");
	
		// Send the first message
		int sent = rPostOffice->Send(outPktHdr, outMailHdr, msg);
	
		if (sent)
			printf("The msg has been received\n");
		else
			printf("FAILD.\n");
	}
	else
	{
		PacketHeader inPktHdr;
		MailHeader inMailHdr;
		char buffer[MaxMailSize];

		rPostOffice->Receive(7, &inPktHdr, &inMailHdr, buffer);
		printf("Got \"%s\" from %d to %d, box %d\n",buffer,inPktHdr.from, inPktHdr.to,inMailHdr.from);
		fflush(stdout);
	}
	interrupt->Halt();
}

void
VariableNetworkTest(int farAddr)
{
	printf("Machine address [%d]\n", 1-farAddr);

	if (farAddr)
	{
		PacketHeader outPktHdr;
		MailHeader outMailHdr;
		const char *msg = "--{\
1234567890123456789012345678901234567890-\
1234567890123456789012345678901234567890-\
1234567890123456789012345678901234567890-\
1234567890123456789012345678901234567890}--";
	
		// construct packet, mail header for original message
		// To: destination machine, mailbox 0
		// From: our machine, reply to: mailbox 1
		outPktHdr.to = farAddr;
		outMailHdr.to = 7;
		outMailHdr.from = 1;
		outMailHdr.length = strlen(msg) + 1;
	
		printf("Sending a loooong msg through an unreliable network\n");
	
		// Send the first message
		int sent = vPostOffice->Send(outPktHdr, outMailHdr, msg);
	
		if (sent)
			printf("The msg has been received\n");
		else
			printf("FAILD.\n");
	}
	else
	{
		PacketHeader inPktHdr;
		MailHeader inMailHdr;
		char buffer[180] = {};

		vPostOffice->Receive(7, &inPktHdr, &inMailHdr, buffer);
		printf("Got \"%s\" from %d to %d, box %d, length %d\n",
			buffer, inPktHdr.from, inPktHdr.to, inMailHdr.from, inMailHdr.length);
		fflush(stdout);
	}
	interrupt->Halt();
}
