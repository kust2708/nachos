/**
 * Automatic test.
 * Need file named "check_list.dat" on the same place of this executable.
 * Each lines are considered as test which must be executed.
 *
 * e.g of check_list.dat file:
 *
 * ./nachos-step3 -x userpages0 -rs 4						<--- test n°1
 * ./nachos-step3 -x userpages0 -rs 3						<--- test n°2
 *
 * last modification le 05/01/15
 */

#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <string.h>

#define TAILLE_MAX 1000 // Tableau de taille 1000

int main()
{
	char cmd[TAILLE_MAX]; // ligne de commande à exécuter
	int ret, check = 1, error = 0; // valeur de retour
	FILE *fichier = NULL; // fichier à lire
	char chaine[TAILLE_MAX] = "", outfile[TAILLE_MAX], orgfile[TAILLE_MAX], tmp_file[TAILLE_MAX]; // Chaîne vide de taille TAILLE_MAX

	fichier = fopen("check_list.dat", "r"); // ouvre le fichier contenant les lignes à exécuter
	
	if (fichier != NULL)
	{
		printf("<< Start to check >>\n\n");
		while(fgets(chaine, TAILLE_MAX, fichier))
		{
			strcpy(cmd,""); // reset the array
			strcat(cmd,chaine); // add the test
			ret = system(cmd); // execute the test
			ret = WEXITSTATUS(ret); // get the return value
			
			if(ret == 0)
			{
				char *buffer, *tmp;
				buffer = strtok(cmd, " ");
				while(buffer != NULL) 
				{
			    	buffer = strtok(NULL, " ");
			    	if(buffer != NULL)
			    	{
				    	tmp = strstr(buffer, ".out");
				    	if(tmp != NULL && strcmp(tmp, ""))
				    	{
				    		tmp = strtok(buffer, "/");
				    		tmp = strtok(NULL, "/");
				    		tmp = strtok(NULL, "/");
				    		buffer = strtok(tmp, ".");
				    		break;
				    	}
			    	}
				}

				//printf("buffer : %s, outfile : %s, orgfile : %s\n", buffer, outfile, orgfile);
				if(buffer)
				{
					sprintf(outfile, "io/%s.out", buffer);
					sprintf(orgfile, "io/%s.origin", buffer);
					sprintf(tmp_file, "io/%s.tmp", buffer);
					
					sprintf(cmd, "head -n -9 %s > %s", outfile, tmp_file);
					system(cmd);
					
					//sprintf(cmd, "diff <(head -n -9 %s) %s", outfile, orgfile);
					sprintf(cmd, "diff %s %s", tmp_file, orgfile);
				}
				else
				{
					printf("cmd : %s\n", cmd);
				}
			}
			check = system(cmd); // execute the test

			printf("\n*****************************************************\n");
			printf("%sStatus : %s\n", chaine, !ret && !check ? "\033[32;01m[OK]\033[00m" : "\033[31;01m[FAILURE]\033[00m");
			printf("*****************************************************\n");

			if(!(!ret && !check))
				error++;
		}
		fclose(fichier); // close the file
		printf("<< End of check >>\n\n");
		if(!error)
			printf("\033[32;07m All tests were terminated successfully. \033[00m\n");
		else
			printf("\033[31;07m Number of errors detected : %d \033[00m \n", error);
		return EXIT_SUCCESS;
	}
	else
	{
		printf("check_list.dat file not found !\n");
		return EXIT_FAILURE;
	}
}
