#include "userprocess.h"

extern int do_UserProcessExit(int pid);


static int pr_count = 1;
static int pid_generator = 1;

static Semaphore* modify_nb_lock = new Semaphore("Current processes number lock", 1);
static Semaphore* creation_lock = new Semaphore("Creation process lock", 1);


/**
 * Generate a new PID
 */
int getIncPid()
{
	creation_lock->P();
	int pid = pid_generator++;
	creation_lock->V();
	return pid;
}

/**
 * Returns the actual number of processes 
*/
int getCurrentProcessesCount() {
	return pr_count;
}

/**
 * Increment the counter (number of processes)
*/
void inc_pr_count()
{
	modify_nb_lock->P();
	pr_count++;
	modify_nb_lock->V();
}

/**
 * Decrement the counter (number of processes)
*/
void dec_pr_count()
{
	modify_nb_lock->P();
	pr_count--;
	modify_nb_lock->V();
}

/**
 * Initialize a new user process 
*/
UserProcess::UserProcess()
{
	bitmap = new BitMap(MAX_THREADS); // stack management
	pid = getIncPid();
	ppid = -1; // by default -1 : must be change with the PID of the parent
	tid_generator = 1; // initialize the first value the TID
	nb_threads = 1; // main already running
	nb_threads_lock = new Semaphore("", 1);
	termination_lock = new Semaphore("", 0);
	creation_lock = new Semaphore("Creation lock", 1);
	map_lock = new Semaphore("Map lock", 1);
}

/**
 * free all pointers : destructor
*/
UserProcess::~UserProcess()
{
	delete map_lock;
	delete bitmap;
	delete nb_threads_lock;
	delete termination_lock;
	delete creation_lock;
}

/**
 * Run the process using an external file
 * filename : the file which must be used
*/
static void StartProcess(char *filename)
{
    OpenFile *executable = fileSystem->Open (filename); // open the file
    AddrSpace *space; // address space pointer

    if (executable == NULL)
    {
	   printf ("Unable to open file %s\n", filename);
	   return;
    }
    space = new AddrSpace (executable); // get the address space of the executable
    currentThread->space = space; // switch the current address space (of the current thread) with the new one

    delete executable;		// close file

    space->InitRegisters ();	// set the initial register values
    space->RestoreState ();	// load page table register

    machine->Run ();		// jump to the user progam
    ASSERT (FALSE);		// machine->Run never returns;
    // the address space exits
    // by doing the syscall "exit"
}

/**
 * Run the process (call StartProcess see above)
 * file : file used by StartProcess which must be runned
*/
static void StartUserProcess(int file) {
	StartProcess((char*) file);
}

/**
 * Clone the current process and call StartUserProcessin order to run the program file
 * file : file used by StartUserProcess which must be runned
*/
int UserProcess::ForkExec(char* file) {

	inc_pr_count(); // increment the pid counter

	// generate the thread's name
	char* tname = new char[64];
	sprintf(tname, "UserProcess [%s]", file);

	ppid = currentThread->getPid(); // get the current thread pid

	Thread *t = new Thread (tname); // allocate a new thread
	t->setPid(pid); // change the pid by default by a new one
	t->setTid(getIncTid()); // change the tid by default by a new one
	t->Fork(StartUserProcess, (int)file); // creation of the new thread

	return 0;
}

/**
 * Execute in the process the program file (see StartProcess)
 * file : file used by StartProcess which must be runned
*/
int UserProcess::Exec(char* file) {

	// generate the thread's name
	char* tname = new char[64];
	sprintf(tname, "UserProcess [%s]", file);
	
	currentThread->setPid(pid);
	StartProcess(file);

	return 0;
}

/**
 * Exit the process, decrement the number of process
*/
int UserProcess::Exit() {
	dec_pr_count();
	termination_lock->V();
	return 0;
}

/**
 * join the process
*/
int UserProcess::Join()
{
	termination_lock->P();
	return 0;
}

/**
 * Check if all children thread are finished,
 * If there are no children left the process exit
*/
int UserProcess::ChildFinished()
{
	dec_nb_threads();

	if (nb_threads == 0) {
		Exit();
		do_UserProcessExit(pid);
	}
	return 0;
}

/**
 * Returns the number of threads currently running
*/
int UserProcess::getNbThreads() {
	return nb_threads;
}

/**
 * Returns the current process id of the process
*/
int UserProcess::getPid() {
	return pid;
}

/**
 * Returns the current parent process id of the process
*/
int UserProcess::getPPid() {
	return ppid;
}

/**
 * Returns the first available stack pointer
*/
int UserProcess::getThreadStackPointer()
{
	return bitmap->Find();
}

/**
 * Checks if a new thread can be added. If it can be, the counter of thread is incremented 
*/
int UserProcess::AddThread()
{
	nb_threads_lock->P();
	if (nb_threads == MAX_THREADS) {
		nb_threads_lock->V();
		return 0;
	}
	nb_threads++;
	nb_threads_lock->V();
	return 1;
}

/**
 * Increment the number of threads
*/
void UserProcess::inc_nb_threads()
{
	nb_threads_lock->P();
	nb_threads++;
	nb_threads_lock->V();
}

/**
 * Decrement the number of threads
*/
void UserProcess::dec_nb_threads()
{
	nb_threads_lock->P();
	nb_threads--;
	nb_threads_lock->V();
}

/**
 * Generate a new TID
 */
int UserProcess::getIncTid()
{
	creation_lock->P();
	int tid = tid_generator++;
	creation_lock->V();
	return tid;
}

/**
 * Initialization of the thread data (join lock, name, tid, ...)
 * tid : thread id
*/
void UserProcess::initThreadJoinFlag(int tid)
{
	// generate the semaphore's name
	char* sname = new char[40];
	sprintf(sname, "Semaphore for UserThread %d", tid);
	Semaphore* sem = new Semaphore(sname, 0);
	thread_data* tmp = new thread_data(-1, sem);
	map_lock->P();
	threadJoinFlag[tid] = tmp;
	map_lock->V();
}

/**
 * Joins the thread, wait for the end of the thread
 * tid : Thread id
*/
int UserProcess::threadJoin(int tid)
{
	threadJoinFlag[tid]->semaphore->P(); // wait for the end of the thread
	map_lock->P();
	int ret = threadJoinFlag[tid]->returnValue;
	delete threadJoinFlag[tid]->semaphore;
	thread_data* tmp = threadJoinFlag[tid];
	threadJoinFlag.erase(tid);
	delete tmp;
	map_lock->V();
	return ret;
}

/**
 * Exit the thread
 * tid : Thread id
 * exitValue : returned value of the thread execution
*/
void UserProcess::threadExit(int tid, int exitValue)
{
	map_lock->P();
	if (threadJoinFlag.count(tid))
	{
		threadJoinFlag[tid]->returnValue = exitValue;
		threadJoinFlag[tid]->semaphore->V(); // signal the waiting thread (see do_UserThreadJoin)
	} else {
		if(tid != 1)
			DEBUG('x', "Can't found data for the thread [%d]\n", currentThread->getTid());
	}
	map_lock->V();
}