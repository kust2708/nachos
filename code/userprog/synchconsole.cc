#include "system.h"
#include "synchconsole.h"


static Semaphore *readAvail = new Semaphore("read avail", 0);
static Semaphore *writeDone = new Semaphore("write done", 0);

static void ReadAvail(int arg)
{
	readAvail->V();
}

static void WriteDone(int arg)
{
	writeDone->V();
}


SynchConsole::SynchConsole(char *readFile, char *writeFile)
{
	console = new Console(readFile, writeFile, ReadAvail, WriteDone, 0);
	readLock = new Semaphore("read lock", 1);
	writeLock = new Semaphore("write lock", 1);
}

SynchConsole::~SynchConsole()
{
	delete writeDone;
	delete readAvail;
	delete readLock;
	delete console;
	delete writeLock;
}

/**
 * Synchronized PutChar, write in the console the character
 * ch : character which must be written
*/
void SynchConsole::SynchPutChar(const char ch)
{
	writeLock->P();
		console->PutChar(ch);
		writeDone->P();
	writeLock->V();
}

/**
 * Synchronized GetChar, get an character from the input device
 * return the retrieved character
*/
char SynchConsole::SynchGetChar()
{
	readLock->P();
	char result;
	readAvail->P();
	result = console->GetChar();
	readLock->V();
	return result;
}

/**
 * Synchronized PutString, write a string in the console
 * s : array of char containing the string which must be displayed
*/
void SynchConsole::SynchPutString(const char s[])
{
	for (unsigned int i = 0, len = strlen(s); i < len; i++) {
		SynchPutChar(s[i]);
	}
}

/**
 * SynchGetString(char *s, int n)
 *
 * s : Pointer to an array of chars where the string read is copied.
 * n : Maximum number of characters to be copied into s (including the terminating null-character).
 *
 * This function mimic the The C library function char *fgets(char *str, int n, FILE *stream) 
 * It reads a line from the specified stream and stores it into the string pointed to by str. 
 * It stops when either (n-1) characters are read, the newline character is read, 
 * or the end-of-file is reached, whichever comes first
 * 
 * we have to ensure thread-safety in this function by surrounding the code by a semaphore.
 *
*/
void SynchConsole::SynchGetString(char *s, int n)
{
	int i;
    for (i=0; i<n-1; i++)
    {
        char c = SynchGetChar();
        if (c == EOF)
            break;
		else if (c == '\n')
		{
		    s[i++] = c;
		    break;
		}
        else s[i] = c;
    }
    s[i] = 0;
}

/**
 * Synchronized PutInt, write an integer in the console
 * n : integer which must be displayed
*/
void SynchConsole::SynchPutInt(const int n)
{
	const int size = 15;
	char buffer[size];
	snprintf(buffer, size, "%d", n);
	SynchPutString(buffer);
}


/**
* Get an integer from the input stream.
* n : pointer to an integer to store the result
* Return the number of read bytes
*
* This function ignore the leading spaces, take the number's sign into and stop parsing
* a number after a EOF, a spase or a non digit character.
*/
int SynchConsole::SynchGetInt(int *n)
{
	const int size = 20;
	char buffer[size] = {};
	int i = 0;
	readAvail->P();
	char c = console->getIncoming();
	// skip spaces
	while (c != EOF && isspace(c)) {
		console->GetChar();
		readAvail->P();
		c = console->getIncoming();
	}
	if (c == '-' || c == '+' || isdigit(c)) {
		buffer[i] = c;
		console->GetChar();
		i++;
		for(; i<size-1; i++)
		{
			readAvail->P();
			c = console->getIncoming();
			if (c == EOF || isspace(c) || !isdigit(c))
			{
				if (!(c == EOF || isspace(c)))
					readAvail->V();
				else
					console->GetChar();
				break;
			}
			console->GetChar();
			buffer[i] = c;
		}
		sscanf(buffer, "%d", n);
		return strlen(buffer);
	} else {
		if (!(c == EOF || isspace(c)))
			readAvail->V();
		return 0;
	}
}
