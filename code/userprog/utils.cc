#include "copyright.h"
#include "system.h"

/**
* Convert a string from the user space to the kernel space
* from : simulated memory address
* to : simulated kernel space
* size : number of bytes to copy (Should be multiple of 4)
*/
void copyStringFromMachine(int from, char *to, unsigned size)
{
	int offset = 0, len;
	do
	{
		char buf[5] = {};
		machine->ReadMem(from, 4, (int*)buf);
		len = strlen(buf);
		memcpy(to + offset, buf, len); 
		from += 4;
		size -= len;
		offset += len;
	} while (len == 4 && size > 0);
	to[offset] = '\0';
}

/**
* Convert a string from the kernel space to the user space
* from : simulated kernel space
* to : simulated memory address
*
* The function copy the null terminator also.
*/
void copyStringToMachine(char* from, int to)
{
	int offset = 0, size = strlen(from);
	while (1)
	{
		int len = size-offset >= 4 ? 4 : size - offset + 1;
		if (len <= 0) break;
		if (len == 3) len = 2;
		machine->WriteMem(to + offset, len, *((int*)(from + offset)));
		offset += len;
	}
}
