#include "system.h"
#include "sysdep.h"
#include "synch.h"


#define N 3

#define MAX_ITR 10
#define NUM_THR 3

int buff[N];

int head = 0;
int size = 0;

Lock* lock;
Condition* full;
Condition* empty;

int x;

void add(int item) {
	lock->Acquire();
	while (size == N) {
		full->Wait(lock);
	}
	buff[(head+size)%N] = item;
	printf(" >>> [%d]\n", item);
	size++;
	empty->Signal(lock);
	lock->Release();
}

int remove() {
	lock->Acquire();
	while (size == 0) {
		empty->Wait(lock);
	}
	int item = buff[head];
	printf(" <<< [%d]\n", item);
	head = (head+1)%N;
	size--;
	x--;
	full->Signal(lock);
	lock->Release();
	return item;
}

void producer(int xx) {
	printf("Producer\n");
	for (int i = 0; i < MAX_ITR; ++i)
	{
		int item = Random()%100;
		printf(" >> [%d]\n", item);
		add(item);
		//currentThread->Yield();
	}
}

void consumer(int xx) {
	printf("Consumer\n");
	for (int i = 0; i < MAX_ITR; ++i)
	{
		int item = remove();
		printf(" << [%d]\n", item);
		//currentThread->Yield();
	}
}

void ProducerComsumerTest() {

	lock = new Lock("lock");
	full = new Condition("full");
	empty = new Condition("empty");
	
	printf("ProducerComsumerTest : Begin\n");
	
	for (int i = 0; i < NUM_THR; ++i)
	{
		(new Thread ("p1"))->Fork(producer, 0);
		(new Thread ("p2"))->Fork(consumer, 0);
	}

	x = MAX_ITR * NUM_THR;
	while (x){ 
		currentThread->Yield(); 
		//printf("%d\n", x);
	}
	
	printf("ProducerComsumerTest : End\n");
	
	delete lock;
	delete full;
	delete empty;
}