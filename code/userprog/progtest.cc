// progtest.cc 
//      Test routines for demonstrating that Nachos can load
//      a user program and execute it.  
//
//      Also, routines for testing the Console hardware device.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
//#include "console.h"
#include "synchconsole.h"
#include "addrspace.h"
#include "synch.h"

extern int do_UserProcessForkExec(char* file);
extern void incrementCurrentThreadsNb();
extern int do_UserProcessJoin(int pid);

//----------------------------------------------------------------------
// StartProcess
//      Run a user program.  Open the executable, load it into
//      memory, and jump to it.
//----------------------------------------------------------------------

void
StartProcess (char *filename)
{
    OpenFile *executable = fileSystem->Open (filename);
    AddrSpace *space;

    if (executable == NULL)
    {
	   printf ("Unable to open file %s\n", filename);
	   return;
    }
    space = new AddrSpace (executable);
    currentThread->space = space;

    delete executable;		// close file

    space->InitRegisters ();	// set the initial register values
    space->RestoreState ();	// load page table register

    machine->Run ();		// jump to the user progam
    ASSERT (FALSE);		// machine->Run never returns;
    // the address space exits
    // by doing the syscall "exit"
}

// Data structures needed for the console test.  Threads making
// I/O requests wait on a Semaphore to delay until the I/O completes.

static Console *console;
static Semaphore *readAvail;
static Semaphore *writeDone;

//----------------------------------------------------------------------
// ConsoleInterruptHandlers
//      Wake up the thread that requested the I/O.
//----------------------------------------------------------------------

static void
ReadAvail (int arg)
{
    readAvail->V ();
}
static void
WriteDone (int arg)
{
    writeDone->V ();
}

//----------------------------------------------------------------------
// ConsoleTest
//      Test the console by echoing characters typed at the input onto
//      the output.  Stop when the user types a 'q'.
//----------------------------------------------------------------------

void
ConsoleTest (char *in, char *out)
{
    if (in != NULL && in[0] == '$') // redirect the input to  the std input (keyboard) 
        in = NULL;
    if (out != NULL && out[0] == '$') // redirect the output to the std output (screen)
        out = NULL;
        
    console = new Console (in, out, ReadAvail, WriteDone, 0);
    readAvail = new Semaphore ("read avail", 0);
    writeDone = new Semaphore ("write done", 0);

    for (;;)
    {
        readAvail->P ();	// wait for character to arrive
        char ch = console->GetChar ();
        if (ch == EOF)
            return;       // if EOF then quit 
        if (ch == 'c')
        {
            console->PutChar('<');
            writeDone->P ();    // wait for write to finish
        }
        console->PutChar (ch);	// echo it!
        writeDone->P ();    // wait for write to finish

        if (ch == 'c')
        {
            console->PutChar('>');
            writeDone->P ();    // wait for write to finish
        }
    }
}


void
SynchConsoleTest(char *in, char *out)
{
    if (in != NULL && in[0] == '$') // redirect the input to  the std input (keyboard) 
        in = NULL;
    if (out != NULL && out[0] == '$') // redirect the output to the std output (screen)
        out = NULL;
    
    char ch;
    SynchConsole *sc = new SynchConsole(in, out);
    while ((ch = sc->SynchGetChar()) != EOF)
        sc->SynchPutChar(ch);
    fprintf(stderr, "Solaris: EOF detected in SynchConsole!\n");
}


void
SynchConsoleTestString(char *in, char *out, unsigned int nb)
{
    if (in != NULL && in[0] == '$') // redirect the input to  the std input (keyboard) 
        in = NULL;
    if (out != NULL && out[0] == '$') // redirect the output to the std output (screen)
        out = NULL;

    SynchConsole *sc = new SynchConsole(in, out);
    char *ch = new char[nb+1];
    ch[nb] = 0;
    do
    {
        sc->SynchGetString(ch, nb);
        sc->SynchPutString(ch);
    }
    while (strlen(ch) == nb);
    fprintf(stderr, "Solaris: EOF detected in SynchConsole!\n");
    delete ch;
}
