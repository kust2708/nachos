#include "copyright.h"
#include "system.h"
#include "frameprovider.h"

#include "sysdep.h"


FrameProvider::FrameProvider() {
	bitmap = new BitMap(NumPhysPages);
	free_frames = NumPhysPages;
	// The main memory is already zero-ed in the Machine constructor
}

FrameProvider::~FrameProvider() {
	delete bitmap;
}

int FrameProvider::GetEmptyFrame() {
	ASSERT (free_frames);
	free_frames--;
	int ind = -1;
#if 1
	ind = bitmap->Find();
#else
	ind = Random()%NumPhysPages;
	while (bitmap->Test(ind))
		ind = Random()%NumPhysPages;
	bitmap->Mark(ind);
#endif
	bzero (machine->mainMemory + PageSize*ind, PageSize);
    DEBUG ('r', "Returned frame = %d\n", ind);
	return ind;
}

void FrameProvider::ReleaseFrame(int frame_id) {
	bitmap->Clear(frame_id);
    DEBUG ('r', "Released frame = %d\n", frame_id);
}

int FrameProvider::NumAvailFrame() {
	return free_frames;
	return bitmap->NumClear();
}
