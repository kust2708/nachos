#include "copyright.h"
#include "system.h"
#include <map>

/**
 * Holds the data requied to support a user-level 
 * Semaphore.
*/
struct UserSem {
	int addr; // the address of the Machine memory of the user-semaphore
	Semaphore* sem; // the actual kernel-level Semaphore
	UserSem(int a, Semaphore* s): addr(a), sem(s) {}
};

// map for fast find operation
std::map<int, UserSem*> mp;

/**
 * Create a kernel-level Semaphore object and link it with the
 * user-level Semaphore referenced by its address int the 
 * machine memory.
 *
*/
void sem_create(int addr, int count) {
	DEBUG('s', "Creating new UserSem: addr=%d, count=%d\n", 
		addr, count);
	if (!mp.count(addr)) {
		Semaphore* sem = new Semaphore("UserSem", count);
		UserSem* userSem = new UserSem(addr, sem);
		mp[addr] = userSem;
	}
}

/**
 * Delete the entry from the map after releasing the semaphore
 * resource.
 * addr : address of the semaphore
*/
void sem_destroy(int addr) {
	if (mp.count(addr)) {
		delete mp[addr]->sem;
		mp.erase(addr);
	}
}

/**
 * Wait for the semaphore specified by the 'addr'
 * addr : address of the semaphore
*/
void sem_down(int addr) {
	DEBUG('s', "Thread %d Wait for semaphore at address : %d\n", currentThread->getTid(), addr);
	UserSem* userSem = mp[addr];
	ASSERT(userSem);
	userSem->sem->P();
}

/**
 * Signal the semaphore specified by the 'addr'
 * addr : address of the semaphore
*/
void sem_up(int addr) {
	DEBUG('s', "Thread %d Signal semaphore at address : %d\n", currentThread->getTid(), addr);
	UserSem* userSem = mp[addr];
	ASSERT(userSem);
	userSem->sem->V();
}
