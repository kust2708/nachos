#include "userthread.h"

extern UserProcess* getUserProcessByID(int pid);


struct userthread_data
{
	int tid; 		// thread's identifier
	int func_addr; 	// func_addr of the routine function
	int func_arg; 	// argument of the routine
	int ret_addr; 	// func_addr of the thread_exit
	/* constructor of the structure */
	userthread_data(int pi, int ad, int ar, int ret):
		tid(pi),func_addr(ad),func_arg(ar),ret_addr(ret){}
};


static Semaphore* mutex = new Semaphore("Current threads number lock", 1);


/**
 * Initialize the new thread and run it
 * f : must contain a func_arg pointer of the new thread (see structure above)
 */
static void StartUserThread(int f) {

	DEBUG('x', "StartUserThread\n");

	userthread_data *str = (userthread_data*) f;
	DEBUG('x', "tid=%d pid=%d\n", str->tid, currentThread->getPid());

    currentThread->space->InitRegisters ();	// set the initial register values
    currentThread->space->RestoreState ();	// load page table register

    machine->WriteRegister(RetAddrReg, str->ret_addr); // specify the func_addr of the code that should be executed at the end
    // it's the address of the ExitUserThread function
    machine->WriteRegister(PCReg, str->func_addr); // write in the register PCReg the function's func_addr 
    machine->WriteRegister(NextPCReg, str->func_addr+4); // write in the register NextPCReg the function's func_addr 
    machine->WriteRegister(4, str->func_arg);


    int pid = currentThread->getPid();
    UserProcess* proc = getUserProcessByID(pid);

    int index = proc->bitmap->Find();
    ASSERT(index != -1);
    DEBUG('x', "Stack index of the current thread - TID : %d, StackIndex : %d \n", currentThread->getTid(), index);
    currentThread->setStackIndex(index);
    
    // compute the func_addr of the new Stack Register    
    int thread_sp = currentThread->space->getTotalAddressSize() - (PageSize * PAGE_PER_THREAD * (index+1)); 
    machine->WriteRegister(StackReg, thread_sp); //initialize the stack register for this current thread
    DEBUG('x', "Thread_sp=%d, Tid=%d\n", thread_sp, str->tid);

	// delete str;
	
    machine->Run ();		// jump to the user progam - never return
}

/**
 * Create a new user thread using the 'f' function with the 'arg' argument associated. 
 * f : Function which must be executed by the new thread (Simulated func_addr of kernel)
 * arg : Arguments of the thread routine
 * return the TID of the new thread
 */
int do_UserThreadCreate(int f, int arg, int ret_addr) 
{
	DEBUG ('x', "do_UserThreadCreate\n");

	mutex->P();

	int pid = currentThread->getPid();
    UserProcess* proc = getUserProcessByID(pid);

    ASSERT(proc);

    if (!proc->AddThread()){
		mutex->V();
    	return -1;
    }

	// create a new pointer of func_arg (see above), getIncTid generate a new TID for the new thread
	userthread_data* argument = new userthread_data(proc->getIncTid(), f, arg, ret_addr);

	// generate the thread's name
	char* tname = new char[32];
	sprintf(tname, "UserThread %d -> Process %d", argument->tid, pid);

	Thread *t = new Thread (tname);
	DEBUG ('x', "New Thread Created [%s]\n", tname);

	t->setTid(argument->tid); // modify the default TID by a new one
	t->setPid(pid);

	t->Fork(StartUserThread, (int)argument);

	proc->initThreadJoinFlag(t->getTid());

	mutex->V();

	return t->getTid();
}

/**
 * Exit the thread and return the parameter value
 * param : value which must be returned
 */
void do_UserThreadExit(int param)
{
	DEBUG('x', "do_UserThreadExit [%d::%d]\n", currentThread->getTid(), currentThread->getPid());

	int tid = currentThread->getTid();
	int index = currentThread->getStackIndex();

	mutex->P();

	int pid = currentThread->getPid();
    UserProcess* proc = getUserProcessByID(pid);

    ASSERT(proc);

	proc->bitmap->Clear(index);

	mutex->V();

	proc->threadExit(tid, param);
	
	proc->ChildFinished();

	currentThread->Finish();	//there is a queue of threads; it is not necessary to be the main one but just the current one.
}

/**
 * Join the thread associated to the TID to the current thread
 * tid : TID of the thread which must be joined
 * return the returned value of the routine (see do_UserThreadCreate)
 */
int do_UserThreadJoin(int tid)
{
	DEBUG('x', "do_UserThreadJoin TID[%d], currentThread : PID[%d] TID[%d]\n", 
		tid, currentThread->getPid(), currentThread->getTid());
	return getUserProcessByID(currentThread->getPid())->threadJoin(tid);
}