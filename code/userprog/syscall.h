/* syscalls.h 
 * 	Nachos system call interface.  These are Nachos kernel operations
 * 	that can be invoked from user programs, by trapping to the kernel
 *	via the "syscall" instruction.
 *
 *	This file is included by user programs and by the Nachos kernel. 
 *
 * Copyright (c) 1992-1993 The Regents of the University of California.
 * All rights reserved.  See copyright.h for copyright notice and limitation 
 * of liability and disclaimer of warranty provisions.
 */

#ifndef SYSCALLS_H
#define SYSCALLS_H

#include "copyright.h"

/* system call codes -- used by the stubs to tell the kernel which system call
 * is being asked for
 */
#define SC_Halt		0
#define SC_Exit		1
#define SC_Exec		2
#define SC_Join		3
#define SC_Create	4
#define SC_Open		5
#define SC_Read		6
#define SC_Write	7
#define SC_Close	8
#define SC_Fork		9
#define SC_Yield	10

// STEP 2 - IO

#define SC_PutChar		20
#define SC_PutString 	21
#define SC_GetChar		22
#define SC_GetString	23
#define SC_PutInt		24
#define SC_GetInt		25
#define SC_Printf		26
#define SC_Scanf		27

// STEP 3 - Multithreading

#define SC_ThreadCreate	30
#define SC_ThreadExit	31
#define SC_ThreadJoin	32
#define SC_SemInit		33
#define SC_P			34
#define SC_V			35
#define SC_SemDestroy	36

// STEP 4 - Virtual memory

#define SC_ForkExec		40


#ifdef IN_USER_MODE

// LB: This part is read only on compiling the test/*.c files.
// It is *not* read on compiling test/start.S


/* The system call interface.  These are the operations the Nachos
 * kernel needs to support, to be able to run user programs.
 *
 * Each of these is invoked by a user program by simply calling the 
 * procedure; an assembly language stub stuffs the system call code
 * into a register, and traps to the kernel.  The kernel procedures
 * are then invoked in the Nachos kernel, after appropriate error checking, 
 * from the system call entry point in exception.cc.
 */

/* Stop Nachos, and print out performance stats */
void Halt () __attribute__((noreturn));


/* Address space control operations: Exit, Exec, and Join */

/* This user program is done (status = 0 means exited normally). */
void Exit (int status) __attribute__((noreturn));

/* A unique identifier for an executing user program (address space) */
typedef int SpaceId;

/* Run the executable, stored in the Nachos file "name", and return the 
 * address space identifier
 */
SpaceId Exec (char *name);

/* Only return once the the user program "id" has finished.  
 * Return the exit status.
 */
int Join (SpaceId id);


/* File system operations: Create, Open, Read, Write, Close
 * These functions are patterned after UNIX -- files represent
 * both files *and* hardware I/O devices.
 *
 * If this assignment is done before doing the file system assignment,
 * note that the Nachos file system has a stub implementation, which
 * will work for the purposes of testing out these routines.
 */

/* A unique identifier for an open Nachos file. */
typedef int OpenFileId;

/* when an address space starts up, it has two open files, representing 
 * keyboard input and display output (in UNIX terms, stdin and stdout).
 * Read and Write can be used directly on these, without first opening
 * the console device.
 */

#define ConsoleInput	0
#define ConsoleOutput	1

/* Create a Nachos file, with "name" */
void Create (char *name);

/* Open the Nachos file "name", and return an "OpenFileId" that can 
 * be used to read and write to the file.
 */
OpenFileId Open (char *name);

/* Write "size" bytes from "buffer" to the open file. */
void Write (char *buffer, int size, OpenFileId id);

/* Read "size" bytes from the open file into "buffer".  
 * Return the number of bytes actually read -- if the open file isn't
 * long enough, or if it is an I/O device, and there aren't enough 
 * characters to read, return whatever is available (for I/O devices, 
 * you should always wait until you can return at least one character).
 */
int Read (char *buffer, int size, OpenFileId id);

/* Close the file, we're done reading and writing to it. */
void Close (OpenFileId id);



/* User-level thread operations: Fork and Yield.  To allow multiple
 * threads to run within a user program. 
 */

/* Fork a thread to run a procedure ("func") in the *same* address space 
 * as the current thread.
 */
void Fork (void (*func) ());

/* Yield the CPU to another runnable thread, whether in this address space 
 * or not. 
 */
void Yield ();


/*
 * Write a character to the std output, schedule an interrupt 
 * to occur in the future, and return.
 * ch : character value which must be displayed.
*/
void SynchPutChar(char ch);

/*
 * Gets the current character value from the std input.
 * wait for the character if it is not available.
*/
char SynchGetChar();

/*
 * Write a string to the simulated display, schedule an interrupt
 * to occur in the futur, and return.
 * str : string value which must be displayed.
*/
void SynchPutString(char* str);


/*
 * Get the string value from the keyboard
 * s : string value which must be displayed.
 * n : size of the string value
*/
void SynchGetString(char *s, int n);


/*
 * Write an integer to the simulated display, schedule an interrupt
 * to occur in the futur, and return.
 * n : integer value which must be displayed.
*/
void SynchPutInt(int n);

/*
 * Gets an integer value from the keyboard
 * n : The pointer which contains the read integer. 
 * Returns the number of read digits. 
*/
int SynchGetInt(int *n);


/*
 * Output a formated string to the standard output
 * The accepted flags
 *    %c for a char
 *    %d for an integer
 *    %s for a string
 * This call can handle up to 3 argument without the format argument,
 * any remaining arguments will be discarded, unknown flags will be
 * discarded too.
 * Returns the number of well written arguments. 
*/
int Printf(const char* format, ...);

/*
 * Reads a formatted string from the standard input.
 * The supported flags are:
 *    %c for a char
 *    %d for an integer
 *    %s for a string
 * This call can handle up to 3 argument without the format argument,
 * any remaining arguments will be discarded, unknown flags will be
 * discarded too. Any wild character unless (%,c,d,s) will be
 * discarded.
 * Returns the number of well read arguments. 
*/
int Scanf(const char* format, ...);

/*
 * Creates a new user-thread
 * f : routine of the thread
 * arg : associated arguments 
 * Returns the thread identifier, -1 otherwise in case of an error
*/
int UserThreadCreate(void f(void *arg), void *arg);

/*
 * Exits the current user-thead.
*/
void UserThreadExit(int param);

/*
 * Join a thread to the current thread, wait for the thread
 * identified by tid to finish.
 * Returns the return value of that thread.
*/
int UserThreadJoin(int tid);

// SEMAPHORE

/*
 * Structure of the user semaphore
*/

typedef int sem_t;

/*
typedef struct _sem_t {
	int x; // useless
} sem_t;
*/

/*
 * Initialize a new semaphore
 * sem : pointer of the new semaphore
 * count : The value argument specifies the initial value for the semaphore
*/
void SemInit(sem_t* sem, int count);

/*
 * Wait 
 * sem : semaphore pointer
*/
void P(sem_t* sem);


/*
 * Signal
 * sem : semaphore pointer
*/
void V(sem_t* sem);

/*
 * Destroy the semaphore
 * sem : semaphore pointer
*/
void SemDestroy(sem_t* sem);

/*
 * Create a new process.
 * path: the path to the execuatable file
 */
int ForkExec(char *path);

#endif // IN_USER_MODE

#endif /* SYSCALL_H */
