#include "copyright.h"
#include "system.h"
#include "synchconsole.h"
#include "userprocess.h"
#include "filesys.h"
#include <string>
#include <dirent.h>
#include <vector>
#include <iostream>
#include <unistd.h>
#include <sstream>


extern int do_UserProcessForkExec(char* file);
extern int do_UserProcessJoin(int pid);
extern void CreateDummyProcess();

extern void Copy (const char *unixFile, const char *nachosFile);
extern void Copy (OpenFile* of, const char *unixFile, const char *nachosFile);
extern void Print (OpenFile* d, const char *file);
extern void NAppend (const char *from, const char *to);
extern void Append (const char *from, const char *to, int half);


bool checkForMoreArguments(int tokSize, int numberArguments)
{
	if(tokSize<numberArguments)
		synchconsole->SynchPutString("More arguments are needed\n");
	return tokSize>=numberArguments;
}

void TinyShell()
{
	CreateDummyProcess();
	//char *cmd = new char[64];
	std::vector<std::string> tokens;
    std::string pwd = "";
    std::string line = "";

#ifdef FILESYS
	OpenFile *currentDirectory = fileSystem->getSystemCurrDir(); //At the beginning it will take the path of the system. 
    pwd = fileSystem->getPath(currentDirectory);
#endif //FILESYS

    while(1)
	{
		printf("%s:$> ", pwd.c_str());
        fflush(stdout);
		
        //synchconsole->SynchGetString(cmd, 64);
        //cmd[strlen(cmd)-1] = 0; // remove the '\n' //-1
        getline(std::cin, line);
		tokens.clear();
		std::stringstream ss(line); // Insert the string into a stream
		std::string buf;
		while (ss >> buf)
			tokens.push_back(buf);

        if (tokens.empty()) continue;

		const char* fcmd = tokens[0].c_str();

		if(!strcmp(fcmd, "exit"))
			break; 
		
#ifdef FILESYS
		else if(!strcmp(fcmd, "help"))
		{
            printf("Welcome with NachOS shell\n");
            printf("Supported commands\n");
            printf("\t\033[34;01mcp\033[00m <unix-file> <nachos-file>\n");
            printf("\t\033[34;01map\033[00m <unix-file> <nachos-file>\n");
            printf("\t\033[34;01mnap\033[00m <NachOS-file> <nachos-file>\n");
            printf("\t\033[34;01mp\033[00m <file>\n");
            printf("\t\033[34;01mmkdir\033[00m <dir_name>\n");
            printf("\t\033[34;01mcd\033[00m <path> \n");
            printf("\t\033[34;01mrm\033[00m <file>\n"); 
            printf("\t\033[34;01mls\033[00m\n");
            printf("\t\033[34;01mexit\033[00m\n");  
		}
		else if(!strcmp(fcmd, "ls"))
		{
			fileSystem->List(currentDirectory);
		}
        else if (!strcmp(fcmd, "cp"))
        {
            // copy from UNIX to Nachos
            if (tokens.size() < 3)
            {
                printf("Usage: cp unix-file nachos-file\n");
            }
            else
            {
                Copy (currentDirectory, tokens[1].c_str(), tokens[2].c_str());
            }
        }
        else if (!strcmp(fcmd, "ap"))
        {
            // append from UNIX to Nachos
            if (tokens.size() < 3)
            {
                printf("Usage: ap unix-file nachos-file\n");
            }
            else
            {
                Append (tokens[1].c_str(), tokens[2].c_str(), 0);
            }   
        }
        else if (!strcmp(fcmd, "nap"))
        {
            // append from UNIX to Nachos
            if (tokens.size() < 3)
            {
                printf("Usage: nap nachOS-file nachos-file\n");
            }
            else
            {
                NAppend (tokens[1].c_str(), tokens[2].c_str());
            }   
        }
		else if (!strcmp(fcmd, "p"))
		{
			// print a Nachos file
			if (tokens.size() < 2)
			{
				printf("Usage: p file\n");
			}
			else
			{
				Print(currentDirectory, tokens[1].c_str());
			}
		}
		else if (!strcmp(fcmd, "dentry"))
		{           
			// list Nachos directory
			if (tokens.size() < 2)
			{
				printf("Usage: dentry file\n");
			}
			else
			{
				fileSystem->PrintHeader ((char *) tokens[1].c_str());
			}
		}
		else if(!strcmp(fcmd, "mkdir"))
		{
			// create new dir in the current directory
			if(tokens.size() < 2)
			{
				printf("Usage: mkdir dir_name\n");
			}
			else
			{			
				int ret = fileSystem->CreateDirectory(currentDirectory,tokens[1].c_str());
                if(ret == 0)
                    printf("The directory [%s] couldn't be created.\n", tokens[1].c_str());
			}
		} 
		else if (!strcmp(fcmd, "cd"))
		{
			// change the current working directory
			if (tokens.size() < 2)
			{
				printf("Usage: cd path \n");
			}
			else
			{
				int ret = fileSystem->ChangeDirectory (&currentDirectory, tokens[1].c_str());

				if(ret == -1)
					printf("There is no such file or directory: %s\n", tokens[1].c_str());
				else if(ret == -2)
					printf("%s is not a directory\n", tokens[1].c_str());

                pwd = fileSystem->getPath(currentDirectory);
			}
		}
		else if (!strcmp(fcmd, "rm"))
		{
			// remove Nachos file
			if (tokens.size() < 2)
			{
				printf("Usage: rm file\n");   
			}
			else
			{
				int ret = fileSystem->Remove(currentDirectory, tokens[1].c_str());
                if(ret == 0)
                    printf("The directory or file [%s] couldn't be removed.\n", tokens[1].c_str());
			}
		}
#endif

#ifndef FILESYS
        else if (access((char*)line.c_str(), F_OK) != -1)
		{
			int pid = do_UserProcessForkExec((char*)line.c_str());
			printf("Jod id = [%d]\n", pid);
			do_UserProcessJoin(pid);
		}
#endif
		else if(!strcmp(fcmd, "lsUnix"))
		{
			DIR *dirp = opendir(".");
			struct dirent *dp;
			while ((dp = readdir(dirp)) != NULL) {
				if (strchr(dp->d_name, '.') == NULL) {
					synchconsole->SynchPutString(dp->d_name);
					synchconsole->SynchPutChar(' ');
				}
			}
			synchconsole->SynchPutChar('\n');
			closedir(dirp);
		}
		else
		{
			printf("File or command not found.\n");
		}
	}
}



