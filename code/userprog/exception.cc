// exception.cc 
//      Entry point into the Nachos kernel from user programs.
//      There are two kinds of things that can cause control to
//      transfer back to here from user code:
//
//      syscall -- The user code explicitly requests to call a procedure
//      in the Nachos kernel.  Right now, the only function we support is
//      "Halt".
//
//      exceptions -- The user code does something that the CPU can't handle.
//      For instance, accessing memory that doesn't exist, arithmetic errors,
//      etc.  
//
//      Interrupts (which can also cause control to transfer from user
//      code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "syscall.h"
#include "userprocess.h"

extern void copyStringToMachine(char* from, int to);
extern void copyStringFromMachine(int from, char *to, unsigned size);

extern int do_UserThreadCreate(int f, int arg, int ret_addr);
extern void do_UserThreadExit(int);
extern int do_UserThreadJoin(int pid);

extern int CanHalt();

extern void sem_create(int addr, int count);
extern void sem_up(int addr);
extern void sem_down(int addr);
extern void sem_destroy(int addr);

extern int do_UserProcessForkExec(char* file); 
extern int do_UserProcessExit();
extern int do_UserProcessJoin(int pid);


//----------------------------------------------------------------------
// UpdatePC : Increments the Program Counter register in order to resume
// the user program immediately after the "syscall" instruction.
//----------------------------------------------------------------------
static void
UpdatePC ()
{
    int pc = machine->ReadRegister (PCReg);
    machine->WriteRegister (PrevPCReg, pc);
    pc = machine->ReadRegister (NextPCReg);
    machine->WriteRegister (PCReg, pc);
    pc += 4;
    machine->WriteRegister (NextPCReg, pc);
}

//----------------------------------------------------------------------
// ExceptionHandler
//      Entry point into the Nachos kernel.  Called when a user program
//      is executing, and either does a syscall, or generates an addressing
//      or arithmetic exception.
//
//      For system calls, the following is the calling convention:
//
//      system call code -- r2
//              arg1 -- r4
//              arg2 -- r5
//              arg3 -- r6
//              arg4 -- r7
//
//      The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//      "which" is the kind of exception.  The list of possible exceptions 
//      are in machine.h.
//----------------------------------------------------------------------

void
ExceptionHandler(ExceptionType which)
{
	int type = machine->ReadRegister(2);
	if(which == SyscallException)
	{
		switch(type)
		{
			case SC_Halt:
			{
				DEBUG('a', "Shutdown, initiated by user program.\n");
				
				interrupt->Halt();
				break;
			}
			case SC_Exit:
			{	
				DEBUG('a', "Shutdown, initiated by user program.\n");
				int reg4 = machine->ReadRegister(4);
				DEBUG('x', "[Returned value from the NachOS user program = %d]\n", reg4);

				interrupt->Halt();
				break;
			}
			case SC_PutChar:
			{
				DEBUG('c', "SC_PutChar exception.\n");
				int reg4 = machine->ReadRegister(4);
				synchconsole->SynchPutChar((char)reg4);
				break;
			}
			case SC_PutString:
			{
				DEBUG('c', "SC_PutString exception.\n");
				int reg4 = machine->ReadRegister(4);
				char* buf = new char[MAX_STRING_SIZE+1];

				do {
					memset(buf, '\0', MAX_STRING_SIZE+1);
					copyStringFromMachine(reg4, buf, MAX_STRING_SIZE);
					synchconsole->SynchPutString(buf);
					reg4 += MAX_STRING_SIZE;
				} while (strlen(buf) == MAX_STRING_SIZE);
				delete [] buf;
				break;
			}
			case SC_GetChar:
			{
				DEBUG('c', "SC_GetChar exception.\n");
				char c = synchconsole->SynchGetChar();
				machine->WriteRegister(2, c);
				break;
			}
			case SC_GetString:
			{
				DEBUG('c', "SC_GetString exception.\n");
				int reg4 = machine->ReadRegister(4);
				int reg5 = machine->ReadRegister(5);
				char* buf = new char[reg5];
				synchconsole->SynchGetString(buf, reg5);
				copyStringToMachine(buf, reg4);
				delete [] buf;
				break;
			}
			case SC_PutInt:
			{
				DEBUG('c', "SC_PutInt exception.\n");
				int reg4 = machine->ReadRegister(4);
				synchconsole->SynchPutInt(reg4);
				break;
			}
			case SC_GetInt:
			{
				DEBUG('c', "SC_GetInt exception.\n");
				int value;
				int bytes = synchconsole->SynchGetInt(&value);
				int reg4 = machine->ReadRegister(4);
				if (bytes != 0)
					machine->WriteMem(reg4, 4, value);				
				machine->WriteRegister(2, bytes);
				break;
			}
			case SC_Printf:
			{
				DEBUG('c', "SC_Printf exception.\n");
				int reg4 = machine->ReadRegister(4);
				char* buf = new char[MAX_STRING_SIZE+1];
				copyStringFromMachine(reg4, buf, MAX_STRING_SIZE);
				int curr_reg = 5, written_args = 0;
				for (int i = 0, len = strlen(buf); i < len; ++i)
				{
					char ch = buf[i];
					if (ch == '%') {
						ch = buf[++i];
						if (curr_reg == 8)
							continue;
						int reg_val = machine->ReadRegister(curr_reg++);
						written_args++;
						switch (ch) {
							case 'c' :
								synchconsole->SynchPutChar((char)reg_val);
								break;
							case 'd' :
								synchconsole->SynchPutInt(reg_val);
								break;
							case 's' : {
								char* str_buf = new char[MAX_STRING_SIZE+1];
								copyStringFromMachine(reg_val, str_buf, MAX_STRING_SIZE);
								synchconsole->SynchPutString(str_buf);
								delete [] str_buf;
								break;
							}
							default : {
								written_args--;
								curr_reg--;
							}
						}
					} else {
						synchconsole->SynchPutChar(ch);
					}
				}
				delete [] buf;
				machine->WriteRegister(2, written_args);
				break;
			}
			case SC_Scanf:
			{
				DEBUG('c', "SC_Scanf exception.\n");
				int reg4 = machine->ReadRegister(4);
				char* buf = new char[MAX_STRING_SIZE+1];
				copyStringFromMachine(reg4, buf, MAX_STRING_SIZE);
				int curr_reg = 5, read_args = 0;
				for (int i = 0, len = strlen(buf); i < len; ++i)
				{
					char ch = buf[i];
					if (ch == '%') {
						ch = buf[++i];
						if (curr_reg == 8)
							continue;
						int reg_val = machine->ReadRegister(curr_reg++);
						read_args++;
						switch (ch) {
							case 'c' : {
								char val = synchconsole->SynchGetChar();
								machine->WriteMem(reg_val, 1, val);
								break;
							}
							case 'd' : {
								int val;
								synchconsole->SynchGetInt(&val);
								machine->WriteMem(reg_val, 4, val);
								break;
							}
							case 's' : {
								char* str_buf = new char[MAX_STRING_SIZE];
								synchconsole->SynchGetString(str_buf, MAX_STRING_SIZE);
								copyStringToMachine(str_buf, reg_val);
								delete [] str_buf;
								break;
							}
							default : {
								read_args--;
								curr_reg--;
							}
						}
					} else {
					}
				}
				delete [] buf;
				machine->WriteRegister(2, read_args);
				break;
			}
			case SC_ThreadCreate:
			{
				DEBUG('a', "SC_ThreadCreate exception.\n");
				int reg4 = machine->ReadRegister(4);
				int reg5 = machine->ReadRegister(5);
				int reg6 = machine->ReadRegister(6);
				int pid = do_UserThreadCreate(reg4, reg5, reg6);
				machine->WriteRegister(2, pid);
				break;
			}
			case SC_ThreadExit:
			{
				DEBUG('a', "SC_ThreadExit exception.\n");
				int reg4 = machine->ReadRegister(4);
				do_UserThreadExit(reg4);
				break;
			}
			case SC_ThreadJoin:
			{
				DEBUG('a', "SC_ThreadJoin exception.\n");
				int reg4 = machine->ReadRegister(4);
				int res = do_UserThreadJoin(reg4);
				machine->WriteRegister(2, res);
				break;
			}
			case SC_SemInit:
			{
				DEBUG('s', "SC_SemInit\n");
				int reg4 = machine->ReadRegister(4);
				int reg5 = machine->ReadRegister(5);
				sem_create(reg4, reg5);					
				break;
			}
			case SC_P:
			{
				DEBUG('s', "SC_P\n");
				int reg4 = machine->ReadRegister(4);
				sem_down(reg4);
				break;
			}
			case SC_V:
			{
				DEBUG('s', "SC_V\n");
				int reg4 = machine->ReadRegister(4);
				sem_up(reg4);
				break;
			}
			case SC_SemDestroy:
			{
				DEBUG('s', "SC_SemDestroy\n");
				int reg4 = machine->ReadRegister(4);
				sem_destroy(reg4);
				break;
			}
			case SC_ForkExec:
			{
				DEBUG('x', "SC_ForkExec\n");
				int reg4 = machine->ReadRegister(4);
				char* buf = new char[MAX_STRING_SIZE+1];
				copyStringFromMachine(reg4, buf, MAX_STRING_SIZE);
				DEBUG('x', "Trying to fork process [%s]\n", buf);
				int res = do_UserProcessForkExec(buf);
				machine->WriteRegister(2, res);
				break;
			}
			case SC_Join:
			{
				DEBUG('x', "SC_Join\n");
				int reg4 = machine->ReadRegister(4);
				int res = do_UserProcessJoin(reg4);
				machine->WriteRegister(2, res);
				break;
			}
			default:
			{
				printf("Unexpected user mode exception %d %d\n", which, type);
				ASSERT(FALSE);
			}
		}
		UpdatePC();
	}
}