#ifndef USERTHREAD_H
#define USERTHREAD_H

#include "copyright.h"
#include "system.h"
#include "userprocess.h"

#define PAGE_PER_THREAD 4

#define MAX_THREADS ((UserStackSize / (PAGE_PER_THREAD * PageSize)) - 1)

#endif // USERTHREAD_H