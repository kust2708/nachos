#include "copyright.h"
#include "system.h"
#include "userprocess.h"
#include <map>

std::map<int, UserProcess*> processes;

/**
 * Create a fork of the process
 * file : file which must be opened and executed
*/
int do_UserProcessForkExec(char* file) {

	UserProcess* proc = new UserProcess();
	DEBUG('p', "do_UserProcessForkExec -- PID = %d\n", proc->getPid());
	processes[proc->getPid()] = proc;
	proc->ForkExec(file);
	return proc->getPid();
}

/**
 * Create a new process and execute it
 * file : file which must be opened and executed
*/
int do_UserProcessExec(char* file) {

	UserProcess* proc = new UserProcess();
	DEBUG('p', "do_UserProcessExec -- PID = %d\n", proc->getPid());
	processes[proc->getPid()] = proc;
	proc->Exec(file);
	return proc->getPid();
}

/**
 * Exit the process to the process id given in parameter
 * pid : process id
*/
int do_UserProcessExit(int pid) {
	DEBUG('p', "do_UserProcessExit -- PID = %d\n", pid);
	
	delete processes[pid];
	processes.erase(pid);

	if (!processes.size()) {
		interrupt->Halt();
	}
	return 1;
}

/**
 * Wait for the end of the thread which match with the process id
 * pid : process id
*/
int do_UserProcessJoin(int pid)
{
	DEBUG('p', "do_UserProcessJoin -- PID = %d\n", pid);
	processes[pid]->Join();
	return 0;
}

/**
 * Return the UserProcess according to the process id
 * pid : Process id
*/
UserProcess* getUserProcessByID(int pid)
{
	return processes[pid];
}

/**
 * Return the current number of processes 
*/
int GetCurrentProcessesNb() 
{
	return processes.size();
}

/**
 * Check if the process can be halted
*/
int CanHalt()
{
	return processes.size() == 1 && processes.begin()->second->getNbThreads() == 1;
}

/**
 * Create a dummy process
*/
void CreateDummyProcess()
{
	UserProcess* proc = new UserProcess();
	processes[proc->getPid()] = proc;
}