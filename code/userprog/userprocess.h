#ifndef USERPROCESS_H
#define USERPROCESS_H

#include "copyright.h"
#include "system.h"
#include "bitmap.h"
#include "userthread.h"
#include <map>


struct thread_data
{
	int returnValue; // return value
	Semaphore* semaphore; // thread semaphore
	/* constructor of the structure */
	thread_data(int ret, Semaphore* sem):returnValue(ret),semaphore(sem){}
};

class UserProcess
{
	public:

		UserProcess();
		~UserProcess();

		int getNbThreads();
		int ForkExec(char* file);
		int Exec(char* file);
		int Exit();
		int Join();

		int AddThread();
		int ChildFinished();

		int getPid();
		int getPPid();
		int getThreadStackPointer();

		int getIncTid();

		void initThreadJoinFlag(int tid);

		int threadJoin(int tid);
		void threadExit(int tid, int exitValue);

		BitMap* bitmap; // stack management

	private:
		int nb_threads; // number of the threads
		int pid; // process id
		int ppid; // parent process pid

		int tid_generator; // sequential generator to threads identifiers

		Semaphore* nb_threads_lock;
		Semaphore* termination_lock;
		Semaphore* creation_lock;
		Semaphore* map_lock;

		std::map<int, thread_data*> threadJoinFlag;

		void inc_nb_threads();
		void dec_nb_threads();
};


#endif // USERPROCESS_H
