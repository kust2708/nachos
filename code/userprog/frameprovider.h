
#ifndef FRAME_PROVIDER_H
#define FRAME_PROVIDER_H

#include "copyright.h"
#include "bitmap.h"

class FrameProvider {

public:
	FrameProvider();
	~FrameProvider();

	int GetEmptyFrame();
	void ReleaseFrame(int frame_id);
	int NumAvailFrame();

private:

	BitMap* bitmap;
	int free_frames; // faster that BitMap::NumClear()
};

#endif // FRAME_PROVIDER_H
