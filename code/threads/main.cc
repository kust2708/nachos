// main.cc 
//      Bootstrap code to initialize the operating system kernel.
//
//      Allows direct calls into internal operating system functions,
//      to simplify debugging and testing.  In practice, the
//      bootstrap code would just initialize data structures,
//      and start a user program to print the login prompt.
//
//      Most of this file is not needed until later assignments.
//
// Usage: nachos -d <debugflags> -rs <random seed #>
//              -s -x <nachos file> -c <consoleIn> <consoleOut>
//              -f -cp <unix file> <nachos file>
//              -p <nachos file> -r <nachos file> -l -D -t
//              -n <network reliability> -m <machine id>
//              -o <other machine id>
//              -z
//
//    -d causes certain debugging messages to be printed (cf. utility.h)
//    -rs causes Yield to occur at random (but repeatable) spots
//    -z prints the copyright message
//
//  USER_PROGRAM
//    -s causes user programs to be executed in single-step mode
//    -x runs a user program
//    -c tests the console
//    -sc tests the synchronous console
//
//  FILESYS
//    -f causes the physical disk to be formatted
//    -cp copies a file from UNIX to Nachos
//    -p prints a Nachos file to stdout
//    -r removes a Nachos file from the file system
//    -l lists the contents of the Nachos directory
//    -D prints the contents of the entire file system 
//    -t tests the performance of the Nachos file system
//
//  NETWORK
//    -n sets the network reliability
//    -m sets this machine's host id (needed for the network)
//    -o runs a simple test of the Nachos network software
//    -r runs a ring test of the Nachos network software
//    -u runs a unrealable test of the Nachos network software
//	  -v runs a variable test of the Nachos network software
//
//  NOTE -- flags are ignored until the relevant assignment.
//  Some of the flags are interpreted here; some in system.cc.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#define MAIN
#include "copyright.h"
#undef MAIN

#include "utility.h"
#include "system.h"

#ifdef USER_PROGRAM
#include "userprocess.h"
#include "userthread.h"
#endif


// External functions used by this file

extern void ThreadTest (void);

extern void Copy (const char *unixFile, const char *nachosFile);
extern void Print (const char *file);
extern void NAppend(const char *from, const char *to);
extern void Append(const char *from, const char *to, int half);
extern void PerformanceTest (void);

extern void StartProcess (char *file), 
	ConsoleTest (char *in, char *out), 
	SynchConsoleTest (char *in, char *out), 
	SynchConsoleTestString (char *in, char *out, unsigned int nb);

extern void MailTest (int networkID);
extern void RingTest (int ring_size);
extern void UnreliableNetworkTest (int networkID);
extern void VariableNetworkTest(int farAddr);
extern void PrintNetworkStatistics();
extern void TestFileTransfer(int machine_addr, int server_addr);

extern void TinyShell();

#ifdef USER_PROGRAM
extern int do_UserProcessExec(char* file);
extern void ProducerComsumerTest();
#endif


//----------------------------------------------------------------------
// main
//      Bootstrap the operating system kernel.  
//      
//      Check command line arguments
//      Initialize data structures
//      (optionally) Call test procedure
//
//      "argc" is the number of command line arguments (including the name
//              of the command) -- ex: "nachos -d +" -> argc = 3 
//      "argv" is an array of strings, one for each command line argument
//              ex: "nachos -d +" -> argv = {"nachos", "-d", "+"}
//----------------------------------------------------------------------

int
main (int argc, char **argv)
{
    int argCount;		// the number of arguments 
    // for a particular command

    DEBUG ('t', "Entering main");
    (void) Initialize (argc, argv);

#ifdef THREADS
    //ThreadTest ();
#endif

    for (argc--, argv++; argc > 0; argc -= argCount, argv += argCount)
	{
		argCount = 1;
		if (!strcmp (*argv, "-z"))	// print copyright
			printf ("%s", copyright);
#ifdef USER_PROGRAM
		if (!strcmp (*argv, "-x"))
		{	// run a user program
			ASSERT (argc > 1);
			//StartProcess (*(argv + 1));
			do_UserProcessExec(*(argv + 1));
			argCount = 2;
		}
		else if (!strcmp (*argv, "-c"))
		{			// test the console
			if (argc == 1)
				ConsoleTest (NULL, NULL);
			else
			{
				ASSERT (argc > 2);
				ConsoleTest (*(argv + 1), *(argv + 2));
				argCount = 3;
			}
			interrupt->Halt ();	// once we start the console, then 
			// Nachos will loop forever waiting for console input
	    }
		else if (!strcmp (*argv, "-sc"))
		{			// test the synchronised console
			if (argc == 1)
				SynchConsoleTest (NULL, NULL);
			else
			{
				ASSERT (argc > 2);
				SynchConsoleTest (*(argv + 1), *(argv + 2));
				argCount = 3;
			}
			interrupt->Halt ();	// once we start the synchronised console, then 
			// Nachos will loop forever waiting for console input
	    }
	    else if (!strcmp (*argv, "-scs"))
		{			// test the console
			if (argc == 2){
				int x = atoi(*(argv + 1));
				ASSERT (x>0)
				SynchConsoleTestString (NULL, NULL, (unsigned int)x);
			}
			else
			{
				ASSERT (argc > 3);
				int x = atoi(*(argv + 1));
				ASSERT (x>0)
				SynchConsoleTestString (*(argv + 2), *(argv + 3), (unsigned int)x);
				argCount = 4;
			}
			interrupt->Halt ();	// once we start the console, then 
			// Nachos will loop forever waiting for console input
	    }
	    else if (!strcmp (*argv, "-shell"))
		{			
			TinyShell();
			interrupt->Halt ();	// once we start the shell, then 
			// Nachos will loop forever waiting for console input
	    } 
	    else if (!strcmp (*argv, "-pcm"))
		{			
			ProducerComsumerTest();
			interrupt->Halt ();	// once we start the shell, then 
			// Nachos will loop forever waiting for console input
		}
		else if (!strcmp (*argv, "-info"))
		{			
			printf("MemorySize = %d\n", MemorySize);
			printf("UserStackSize = %d\n", UserStackSize);
			printf("MAX_THREADS = %d\n", MAX_THREADS);
			printf("PAGE_PER_THREAD = %d\n", PAGE_PER_THREAD);
			printf("MAX_PR = %d\n", MemorySize/UserStackSize/2);
			interrupt->Halt ();
		}
#endif // USER_PROGRAM

#ifdef FILESYS
	  if (!strcmp (*argv, "-cp"))
	    {			// copy from UNIX to Nachos
			ASSERT (argc > 2);
			Copy (*(argv + 1), *(argv + 2));
			argCount = 3;
	    }
	  else if (!strcmp (*argv, "-ap"))
	    {			// copy from UNIX to Nachos
			ASSERT (argc > 2);
			Append (*(argv + 1), *(argv + 2), 0);
			argCount = 3;
	    }
	  else if (!strcmp (*argv, "-nap"))
	    {			// copy from UNIX to Nachos
			ASSERT (argc > 2);
			NAppend (*(argv + 1), *(argv + 2));
			argCount = 3;
	    }
	  else if (!strcmp (*argv, "-p"))
	    {			// print a Nachos file
			ASSERT (argc > 1);
			Print (*(argv + 1));
			argCount = 2;
	    }
	  else if (!strcmp (*argv, "-r"))
	    {			// remove Nachos file
			ASSERT (argc > 1);
			fileSystem->Remove (*(argv + 1));
			argCount = 2;
	    }
	  else if (!strcmp (*argv, "-l"))
	    {			// list Nachos directory
			fileSystem->List ();
	    }
	  else if (!strcmp (*argv, "-D"))
	    {			// print entire filesystem
			fileSystem->Print ();
	    }
	  else if (!strcmp (*argv, "-t"))
	    {			// performance test
			PerformanceTest ();
	    }
	    else if (!strcmp (*argv, "-dentry"))
	    {			// list Nachos directory
			ASSERT (argc > 1);
			fileSystem->PrintHeader (*(argv + 1));
			argCount = 2;
	    }
	    else if (!strcmp (*argv, "-mkdir"))
	    {			// list Nachos directory
			ASSERT (argc > 1);
			fileSystem->CreateDirectory (*(argv + 1));
			argCount = 2;
	    }
	    else if (!strcmp (*argv, "-cd"))
	    {			// list Nachos directory
			ASSERT (argc > 1);
			fileSystem->ChangeDirectory (*(argv + 1));
			argCount = 2;
	    }
#endif // FILESYS
#ifdef NETWORK
		if (!strcmp (*argv, "-info"))
		{
			PrintNetworkStatistics();
			interrupt->Halt ();
		}
		if (!strcmp (*argv, "-o"))
		{
			ASSERT (argc > 1);
			Delay (1);	// delay for 2 seconds
			// to give the user time to 
			// start up another nachos
			MailTest (atoi (*(argv + 1)));
			argCount = 2;
		}
		else if (!strcmp (*argv, "-r"))
		{
			ASSERT (argc > 1);
			Delay (1);	// delay for 2 seconds
			// to give the user time to 
			// start up another nachos
			RingTest (atoi (*(argv + 1)));
			argCount = 2;
		}
		else if (!strcmp (*argv, "-u"))
		{
			ASSERT (argc > 1);
			Delay (1);	// delay for 2 seconds
			// to give the user time to 
			// start up another nachos
			UnreliableNetworkTest (atoi (*(argv + 1)));
			argCount = 2;
		}
		else if (!strcmp (*argv, "-v"))
		{
			ASSERT (argc > 1);
			Delay (1);	// delay for 2 seconds
			// to give the user time to 
			// start up another nachos
			VariableNetworkTest (atoi (*(argv + 1)));
			argCount = 2;
		}
		else if (!strcmp (*argv, "-f"))
		{
			ASSERT (argc > 2);
			Delay (1);	// delay for 2 seconds
			// to give the user time to 
			// start up another nachos
			TestFileTransfer(atoi (*(argv + 1)), atoi (*(argv + 2)));
			argCount = 2;
		}
#endif // NETWORK
      }

      interrupt->Halt();
      
	currentThread->Finish();	// NOTE: if the procedure "main" 
	// returns, then the program "nachos"
	// will exit (as any other normal program
	// would).  But there may be other
	// threads on the ready list.  We switch
	// to those threads by saying that the
	// "main" thread is finished, preventing
	// it from returning.
    return (0);			// Not reached...
}
